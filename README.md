Installation
============

 - RTFM :D
 - run 'composer update' (if global) or 'php composer.phar update'
 - php 'php bin/console doctrine:schema:update --force'
 - run 'php bin/console doctrine:migrations:migrate'
 - run 'php bin/console doctrine:fixtures:load --append'
 - run server ... see below

* for 'prod' environment modify web/.htaccess and make sure app.dev is being used (instead of app_dev.php) 

Run server
============
 - php bin/console server:run

Utils
============
 - php bin/console assets:install --symlink
 - php bin/console cache:clear (when new css/js file added to bundle)

Our friends
============
 - Symfony: https://symfony.com/doc/current/index.html
 - Bootstrap: http://getbootstrap.com/css/
 - Twig: http://twig.sensiolabs.org/documentation