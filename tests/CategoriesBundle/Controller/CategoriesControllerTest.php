<?php

namespace CategoriesBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class CategoriesControllerTest extends WebTestCase {

    protected $id;
    
    public function testCRUD() {
        // Create a new client to browse the application
        $client = static::createClient();
        # list
        $url = '/categories/';
        // makes the POST request
        $crawler = $client->request('GET', $url);
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        # add
        $url = '/categories/new';
        // makes the POST request
        $crawler = $client->request('POST', $url, array(
            'category' => 'Test'), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
                )
        );
        
        $response = $client->getResponse()->getContent();
        $result = json_decode($response);
        $this->id = isset($result) ? $result->last_id : 3;
        
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        
        # edit
        
        $url = '/categories/new';
        // makes the POST request
        $crawler = $client->request('POST', $url, array(
            'id' => $this->id, 'category' => 'Test modificat'), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
                )
        );
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        
        # delete
        $url = '/categories/remove/'.$this->id;
        // makes the POST request
        $crawler = $client->request('DELETE', $url, array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
                )
        );
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }
}
