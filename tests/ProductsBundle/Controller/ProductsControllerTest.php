<?php

namespace ProductsBundle\Tests\Controller;

use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

class ProductsControllerTest extends WebTestCase
{
    private $id;
 
    public function testCRUD() {
        // Create a new client to browse the application
        $client = static::createClient();
        
        $url = '/products/';
        // makes the POST request
        $crawler = $client->request('GET', $url);
        $this->assertSame(200, $client->getResponse()->getStatusCode());

        $url = '/products/new';
        // makes the POST request
        $crawler = $client->request('POST', $url, array(
            'category_id' => 1, 'product' => 'Product test', 'price' => 23.2345), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
                )
        );
        $response = $client->getResponse()->getContent();
        $result = json_decode($response);
        $this->id = isset($result) ? $result->last_id : 1;
       
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        
        # edit product
        $url = '/products/new';
        // makes the POST request
        $crawler = $client->request('POST', $url, array(
            'id' => $this->id, 'category_id' => 1, 'product' => 'Product test edited', 'price' => 25.0002), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
                )
        );
        $this->assertSame(200, $client->getResponse()->getStatusCode());
        
        # delete
        $url = '/products/remove/'.$this->id;
        // makes the POST request
        $crawler = $client->request('DELETE', $url, array(), array(), array(
            'HTTP_X-Requested-With' => 'XMLHttpRequest',
                )
        );
        $this->assertSame(200, $client->getResponse()->getStatusCode());
    }
}
