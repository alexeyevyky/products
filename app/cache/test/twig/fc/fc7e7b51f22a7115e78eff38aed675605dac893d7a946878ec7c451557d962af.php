<?php

/* base.html.twig */
class __TwigTemplate_253c43d3d5ef588cc40f65d19118330e85efae99cbcb55ad10c4d23cf27ab24a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_8b2a858e0f1349c0d4ff77a68138807d3a8046257e5419776501eafd8d87b1ae = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8b2a858e0f1349c0d4ff77a68138807d3a8046257e5419776501eafd8d87b1ae->enter($__internal_8b2a858e0f1349c0d4ff77a68138807d3a8046257e5419776501eafd8d87b1ae_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
    ";
        // line 14
        $this->displayBlock('body', $context, $blocks);
        // line 15
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 20
        echo "</body>
</html>
";
        
        $__internal_8b2a858e0f1349c0d4ff77a68138807d3a8046257e5419776501eafd8d87b1ae->leave($__internal_8b2a858e0f1349c0d4ff77a68138807d3a8046257e5419776501eafd8d87b1ae_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_6bdd57492bf049bef5a2c73d6929f9744a5cb05f2352d07cbdf9eb1854cd4240 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6bdd57492bf049bef5a2c73d6929f9744a5cb05f2352d07cbdf9eb1854cd4240->enter($__internal_6bdd57492bf049bef5a2c73d6929f9744a5cb05f2352d07cbdf9eb1854cd4240_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_6bdd57492bf049bef5a2c73d6929f9744a5cb05f2352d07cbdf9eb1854cd4240->leave($__internal_6bdd57492bf049bef5a2c73d6929f9744a5cb05f2352d07cbdf9eb1854cd4240_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_98985d53c0e982c74ecc179aeb487a5f45877e9c505659068bfec38f597724c4 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_98985d53c0e982c74ecc179aeb487a5f45877e9c505659068bfec38f597724c4->enter($__internal_98985d53c0e982c74ecc179aeb487a5f45877e9c505659068bfec38f597724c4_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/css/style.css"), "html", null, true);
        echo "\" />
            
        ";
        
        $__internal_98985d53c0e982c74ecc179aeb487a5f45877e9c505659068bfec38f597724c4->leave($__internal_98985d53c0e982c74ecc179aeb487a5f45877e9c505659068bfec38f597724c4_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_6c18ef3f3d18620b322cbfdf0094614ee5675d4b6814757de3da025eb2e62a91 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6c18ef3f3d18620b322cbfdf0094614ee5675d4b6814757de3da025eb2e62a91->enter($__internal_6c18ef3f3d18620b322cbfdf0094614ee5675d4b6814757de3da025eb2e62a91_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_6c18ef3f3d18620b322cbfdf0094614ee5675d4b6814757de3da025eb2e62a91->leave($__internal_6c18ef3f3d18620b322cbfdf0094614ee5675d4b6814757de3da025eb2e62a91_prof);

    }

    // line 15
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_0d8216a09c6855f0546f9b157771a720571c4b8697777f8625f6f164fe97ba36 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0d8216a09c6855f0546f9b157771a720571c4b8697777f8625f6f164fe97ba36->enter($__internal_0d8216a09c6855f0546f9b157771a720571c4b8697777f8625f6f164fe97ba36_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 16
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/tether.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    ";
        
        $__internal_0d8216a09c6855f0546f9b157771a720571c4b8697777f8625f6f164fe97ba36->leave($__internal_0d8216a09c6855f0546f9b157771a720571c4b8697777f8625f6f164fe97ba36_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 18,  114 => 17,  109 => 16,  103 => 15,  92 => 14,  82 => 8,  77 => 7,  71 => 6,  59 => 5,  50 => 20,  47 => 15,  45 => 14,  38 => 11,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/categories/css/bootstrap.min.css') }}\" />
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/categories/css/style.css') }}\" />
            
        {% endblock  %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
    {% block body %}{% endblock %}
    {% block javascripts %}
        <script src=\"{{ asset('bundles/categories/js/jquery.min.js') }}\"></script>
        <script src=\"{{ asset('bundles/categories/js/tether.min.js') }}\"></script>
        <script src=\"{{ asset('bundles/categories/js/bootstrap.min.js') }}\"></script>
    {% endblock  %}
</body>
</html>
", "base.html.twig", "C:\\xampp\\htdocs\\products\\app\\Resources\\views\\base.html.twig");
    }
}
