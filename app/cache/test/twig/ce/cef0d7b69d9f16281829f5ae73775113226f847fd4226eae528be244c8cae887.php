<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_2ec2821dacbf58ad408add18c75ebf724023c50aba0683b9de785e26711ac144 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_68a98d28319399c69c4f062465c0acb6a93beab8b3b725bfa25c717805809223 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_68a98d28319399c69c4f062465c0acb6a93beab8b3b725bfa25c717805809223->enter($__internal_68a98d28319399c69c4f062465c0acb6a93beab8b3b725bfa25c717805809223_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_68a98d28319399c69c4f062465c0acb6a93beab8b3b725bfa25c717805809223->leave($__internal_68a98d28319399c69c4f062465c0acb6a93beab8b3b725bfa25c717805809223_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_5374904a8c7eefbc3817419bd7a1c4783afe8f3866180c2988c5f68169cf0931 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5374904a8c7eefbc3817419bd7a1c4783afe8f3866180c2988c5f68169cf0931->enter($__internal_5374904a8c7eefbc3817419bd7a1c4783afe8f3866180c2988c5f68169cf0931_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_5374904a8c7eefbc3817419bd7a1c4783afe8f3866180c2988c5f68169cf0931->leave($__internal_5374904a8c7eefbc3817419bd7a1c4783afe8f3866180c2988c5f68169cf0931_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_731b532b825831ea5a63123da0df3a2d67fa24721de48f1dadea91a8505d9a26 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_731b532b825831ea5a63123da0df3a2d67fa24721de48f1dadea91a8505d9a26->enter($__internal_731b532b825831ea5a63123da0df3a2d67fa24721de48f1dadea91a8505d9a26_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_731b532b825831ea5a63123da0df3a2d67fa24721de48f1dadea91a8505d9a26->leave($__internal_731b532b825831ea5a63123da0df3a2d67fa24721de48f1dadea91a8505d9a26_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_c4624c14d0f12436b83e15ffe17305f030277e444c8822edf58fda55f07019d8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c4624c14d0f12436b83e15ffe17305f030277e444c8822edf58fda55f07019d8->enter($__internal_c4624c14d0f12436b83e15ffe17305f030277e444c8822edf58fda55f07019d8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_c4624c14d0f12436b83e15ffe17305f030277e444c8822edf58fda55f07019d8->leave($__internal_c4624c14d0f12436b83e15ffe17305f030277e444c8822edf58fda55f07019d8_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
