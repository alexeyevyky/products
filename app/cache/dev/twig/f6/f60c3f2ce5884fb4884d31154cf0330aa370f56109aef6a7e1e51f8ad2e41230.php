<?php

/* products/new.html.twig */
class __TwigTemplate_bfc815ecfe081f6f90e308aaa2dde86bf66232ec87b6662d30a218701cd6955c extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "products/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_0b903af5d1d04775e3b4432e0e4c28c52f90b2ac3d05fe461316a314446498fd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b903af5d1d04775e3b4432e0e4c28c52f90b2ac3d05fe461316a314446498fd->enter($__internal_0b903af5d1d04775e3b4432e0e4c28c52f90b2ac3d05fe461316a314446498fd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "products/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_0b903af5d1d04775e3b4432e0e4c28c52f90b2ac3d05fe461316a314446498fd->leave($__internal_0b903af5d1d04775e3b4432e0e4c28c52f90b2ac3d05fe461316a314446498fd_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_243b5ca585f3cfcd3934a12952fd2d29b272c142bd25a52fa1a96e3a71140be8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_243b5ca585f3cfcd3934a12952fd2d29b272c142bd25a52fa1a96e3a71140be8->enter($__internal_243b5ca585f3cfcd3934a12952fd2d29b272c142bd25a52fa1a96e3a71140be8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Product creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_243b5ca585f3cfcd3934a12952fd2d29b272c142bd25a52fa1a96e3a71140be8->leave($__internal_243b5ca585f3cfcd3934a12952fd2d29b272c142bd25a52fa1a96e3a71140be8_prof);

    }

    public function getTemplateName()
    {
        return "products/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 13,  53 => 9,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Product creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('products_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", "products/new.html.twig", "C:\\xampp\\htdocs\\products\\app\\Resources\\views\\products\\new.html.twig");
    }
}
