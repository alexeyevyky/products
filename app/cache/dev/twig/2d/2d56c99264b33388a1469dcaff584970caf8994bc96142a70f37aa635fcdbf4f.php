<?php

/* @Framework/Form/container_attributes.html.php */
class __TwigTemplate_9c905872630d0d1b74c0f153532db5154ce0bafbe23482194cacb0312113fab2 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_790d66140f3aaa2848605052ffe4030a7f12ed7dcaa6ba3669e3e3479ce46e82 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_790d66140f3aaa2848605052ffe4030a7f12ed7dcaa6ba3669e3e3479ce46e82->enter($__internal_790d66140f3aaa2848605052ffe4030a7f12ed7dcaa6ba3669e3e3479ce46e82_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/container_attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
";
        
        $__internal_790d66140f3aaa2848605052ffe4030a7f12ed7dcaa6ba3669e3e3479ce46e82->leave($__internal_790d66140f3aaa2848605052ffe4030a7f12ed7dcaa6ba3669e3e3479ce46e82_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/container_attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_container_attributes') ?>
", "@Framework/Form/container_attributes.html.php", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\container_attributes.html.php");
    }
}
