<?php

/* @Framework/Form/attributes.html.php */
class __TwigTemplate_3b7f7e712b5231ced481f7c64d5c3fa39674d52a20b96512080f1318dd0b115e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7109413dd2a6c0709fd492c6ef11cdd807cd79ed57fd5f3a81d8374799de7b80 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7109413dd2a6c0709fd492c6ef11cdd807cd79ed57fd5f3a81d8374799de7b80->enter($__internal_7109413dd2a6c0709fd492c6ef11cdd807cd79ed57fd5f3a81d8374799de7b80_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/attributes.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
";
        
        $__internal_7109413dd2a6c0709fd492c6ef11cdd807cd79ed57fd5f3a81d8374799de7b80->leave($__internal_7109413dd2a6c0709fd492c6ef11cdd807cd79ed57fd5f3a81d8374799de7b80_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/attributes.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'widget_attributes') ?>
", "@Framework/Form/attributes.html.php", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\attributes.html.php");
    }
}
