<?php

/* categories/new.html.twig */
class __TwigTemplate_ba0a5d7995831cbe963728857867f68e427044ac6a1cc26714d6add3532f8e7f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "categories/new.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ff746e398b251ee81dd2fc9b7df403dff061f169556fc0639c7f333133f1aaf3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ff746e398b251ee81dd2fc9b7df403dff061f169556fc0639c7f333133f1aaf3->enter($__internal_ff746e398b251ee81dd2fc9b7df403dff061f169556fc0639c7f333133f1aaf3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "categories/new.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ff746e398b251ee81dd2fc9b7df403dff061f169556fc0639c7f333133f1aaf3->leave($__internal_ff746e398b251ee81dd2fc9b7df403dff061f169556fc0639c7f333133f1aaf3_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_dea46772618837b4826cde01e247f63f2731dff4955111b1a4c4d839ab95f7b3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_dea46772618837b4826cde01e247f63f2731dff4955111b1a4c4d839ab95f7b3->enter($__internal_dea46772618837b4826cde01e247f63f2731dff4955111b1a4c4d839ab95f7b3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Category creation</h1>

    ";
        // line 6
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_start');
        echo "
        ";
        // line 7
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->searchAndRenderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'widget');
        echo "
        <input type=\"submit\" value=\"Create\" />
    ";
        // line 9
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["form"]) ? $context["form"] : $this->getContext($context, "form")), 'form_end');
        echo "

    <ul>
        <li>
            <a href=\"";
        // line 13
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_index");
        echo "\">Back to the list</a>
        </li>
    </ul>
";
        
        $__internal_dea46772618837b4826cde01e247f63f2731dff4955111b1a4c4d839ab95f7b3->leave($__internal_dea46772618837b4826cde01e247f63f2731dff4955111b1a4c4d839ab95f7b3_prof);

    }

    public function getTemplateName()
    {
        return "categories/new.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  60 => 13,  53 => 9,  48 => 7,  44 => 6,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Category creation</h1>

    {{ form_start(form) }}
        {{ form_widget(form) }}
        <input type=\"submit\" value=\"Create\" />
    {{ form_end(form) }}

    <ul>
        <li>
            <a href=\"{{ path('categories_index') }}\">Back to the list</a>
        </li>
    </ul>
{% endblock %}
", "categories/new.html.twig", "C:\\xampp\\htdocs\\products\\app\\Resources\\views\\categories\\new.html.twig");
    }
}
