<?php

/* default/index.html.twig */
class __TwigTemplate_b3fda2193bd02ac3bd168482444677547593ce804160dc9c96560306bb299abe extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "default/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_b3f5a1c889193a1c0ae5f94a804646f3aa722e52872c3a46f4b8a84f6906ffa9 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b3f5a1c889193a1c0ae5f94a804646f3aa722e52872c3a46f4b8a84f6906ffa9->enter($__internal_b3f5a1c889193a1c0ae5f94a804646f3aa722e52872c3a46f4b8a84f6906ffa9_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "default/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_b3f5a1c889193a1c0ae5f94a804646f3aa722e52872c3a46f4b8a84f6906ffa9->leave($__internal_b3f5a1c889193a1c0ae5f94a804646f3aa722e52872c3a46f4b8a84f6906ffa9_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_891aeb154df67361ffba12120932e5edfb04a8abece3a3a440e0aaccc59078dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_891aeb154df67361ffba12120932e5edfb04a8abece3a3a440e0aaccc59078dd->enter($__internal_891aeb154df67361ffba12120932e5edfb04a8abece3a3a440e0aaccc59078dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"container\" id=\"container\">
        <nav class=\"navbar navbar-default\">
            <div class=\"container-fluid\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a class=\"navbar-brand\" href=\"#\">VE</a>
                </div>
                <div id=\"navbar\" class=\"navbar-collapse collapse\">
                    <ul class=\"nav navbar-nav\">
                        <li class=\"active\"><a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_index");
        echo "\">Categories</a></li>
                        <li><a href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_index");
        echo "\">Products</a></li>             
                    </ul>           
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

        <h1>PHP Practical Test</h1>
    </div>
";
        
        $__internal_891aeb154df67361ffba12120932e5edfb04a8abece3a3a440e0aaccc59078dd->leave($__internal_891aeb154df67361ffba12120932e5edfb04a8abece3a3a440e0aaccc59078dd_prof);

    }

    public function getTemplateName()
    {
        return "default/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 20,  60 => 19,  56 => 18,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"container\" id=\"container\">
        <nav class=\"navbar navbar-default\">
            <div class=\"container-fluid\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a class=\"navbar-brand\" href=\"#\">VE</a>
                </div>
                <div id=\"navbar\" class=\"navbar-collapse collapse\">
                    <ul class=\"nav navbar-nav\">
                        <li class=\"active\"><a href=\"{{path('homepage')}}\">Home</a></li>
                        <li><a href=\"{{path('categories_index')}}\">Categories</a></li>
                        <li><a href=\"{{path('products_index')}}\">Products</a></li>             
                    </ul>           
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

        <h1>PHP Practical Test</h1>
    </div>
{% endblock %}", "default/index.html.twig", "C:\\xampp\\htdocs\\products\\app\\Resources\\views\\default\\index.html.twig");
    }
}
