<?php

/* @WebProfiler/Profiler/ajax_layout.html.twig */
class __TwigTemplate_6402585cd7adf8e16a420c4d4493112bb068585eef9ded34c965c313ffabd640 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7959411e3941af64599ad1b27e70b6f9d33647f029b6775fb2a110e0a03b76ef = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7959411e3941af64599ad1b27e70b6f9d33647f029b6775fb2a110e0a03b76ef->enter($__internal_7959411e3941af64599ad1b27e70b6f9d33647f029b6775fb2a110e0a03b76ef_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_7959411e3941af64599ad1b27e70b6f9d33647f029b6775fb2a110e0a03b76ef->leave($__internal_7959411e3941af64599ad1b27e70b6f9d33647f029b6775fb2a110e0a03b76ef_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_6a2e0a5e88a67818517c54851d8b59d247a6382eefe0fa044be727e4df58e297 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6a2e0a5e88a67818517c54851d8b59d247a6382eefe0fa044be727e4df58e297->enter($__internal_6a2e0a5e88a67818517c54851d8b59d247a6382eefe0fa044be727e4df58e297_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_6a2e0a5e88a67818517c54851d8b59d247a6382eefe0fa044be727e4df58e297->leave($__internal_6a2e0a5e88a67818517c54851d8b59d247a6382eefe0fa044be727e4df58e297_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "@WebProfiler/Profiler/ajax_layout.html.twig", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\ajax_layout.html.twig");
    }
}
