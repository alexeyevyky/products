<?php

/* @Framework/Form/number_widget.html.php */
class __TwigTemplate_dc0163fc7f07d3d45cd10599c1be18c634698bc7bb12401bb115481e297db2b6 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e1363b0a96244c4eb3b7c34b48acedb10f6eec46d3a0d3f7e9f0b6735f4abd38 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e1363b0a96244c4eb3b7c34b48acedb10f6eec46d3a0d3f7e9f0b6735f4abd38->enter($__internal_e1363b0a96244c4eb3b7c34b48acedb10f6eec46d3a0d3f7e9f0b6735f4abd38_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/number_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
";
        
        $__internal_e1363b0a96244c4eb3b7c34b48acedb10f6eec46d3a0d3f7e9f0b6735f4abd38->leave($__internal_e1363b0a96244c4eb3b7c34b48acedb10f6eec46d3a0d3f7e9f0b6735f4abd38_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/number_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'text')) ?>
", "@Framework/Form/number_widget.html.php", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\number_widget.html.php");
    }
}
