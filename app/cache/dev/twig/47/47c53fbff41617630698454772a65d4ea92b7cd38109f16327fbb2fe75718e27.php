<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_0583871522995814d7195b69670d1d89a2be8fbb9326a2e988a05bb46a4110bb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7d40a9d8dfcf1a6a4c5c2a56f3bbf81547bfd51cdb8e2fa528683587d4ddbe13 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7d40a9d8dfcf1a6a4c5c2a56f3bbf81547bfd51cdb8e2fa528683587d4ddbe13->enter($__internal_7d40a9d8dfcf1a6a4c5c2a56f3bbf81547bfd51cdb8e2fa528683587d4ddbe13_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_7d40a9d8dfcf1a6a4c5c2a56f3bbf81547bfd51cdb8e2fa528683587d4ddbe13->leave($__internal_7d40a9d8dfcf1a6a4c5c2a56f3bbf81547bfd51cdb8e2fa528683587d4ddbe13_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_ed1361cda9b6e306619890e6d860539e034d927437660e4bee7632dee4819531 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ed1361cda9b6e306619890e6d860539e034d927437660e4bee7632dee4819531->enter($__internal_ed1361cda9b6e306619890e6d860539e034d927437660e4bee7632dee4819531_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_ed1361cda9b6e306619890e6d860539e034d927437660e4bee7632dee4819531->leave($__internal_ed1361cda9b6e306619890e6d860539e034d927437660e4bee7632dee4819531_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_efa421ce3867822accd5ccf20b814bdede34b79f297e8270834cae8a84664e75 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_efa421ce3867822accd5ccf20b814bdede34b79f297e8270834cae8a84664e75->enter($__internal_efa421ce3867822accd5ccf20b814bdede34b79f297e8270834cae8a84664e75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_efa421ce3867822accd5ccf20b814bdede34b79f297e8270834cae8a84664e75->leave($__internal_efa421ce3867822accd5ccf20b814bdede34b79f297e8270834cae8a84664e75_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_bcd3f83ac2054137f59605840a20bc4f16b0e0f284264d562aca8d4a202ba229 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_bcd3f83ac2054137f59605840a20bc4f16b0e0f284264d562aca8d4a202ba229->enter($__internal_bcd3f83ac2054137f59605840a20bc4f16b0e0f284264d562aca8d4a202ba229_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_bcd3f83ac2054137f59605840a20bc4f16b0e0f284264d562aca8d4a202ba229->leave($__internal_bcd3f83ac2054137f59605840a20bc4f16b0e0f284264d562aca8d4a202ba229_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
