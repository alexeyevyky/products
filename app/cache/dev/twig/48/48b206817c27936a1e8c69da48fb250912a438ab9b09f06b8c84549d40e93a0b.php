<?php

/* WebProfilerBundle:Profiler:toolbar_redirect.html.twig */
class __TwigTemplate_dccb185b1e75b829cdb1e1f9fb9e7caadd7babcdf22f96e13cd497d42bf9596a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fd94c4749d82589d959a599e4569ada65440f61fd8f756bab17e440c21dbf1bf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd94c4749d82589d959a599e4569ada65440f61fd8f756bab17e440c21dbf1bf->enter($__internal_fd94c4749d82589d959a599e4569ada65440f61fd8f756bab17e440c21dbf1bf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_fd94c4749d82589d959a599e4569ada65440f61fd8f756bab17e440c21dbf1bf->leave($__internal_fd94c4749d82589d959a599e4569ada65440f61fd8f756bab17e440c21dbf1bf_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_e2eb85607cad2b1c36168a8aaafc8a5ab696a6542454876c7b8ee17a365ce87b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e2eb85607cad2b1c36168a8aaafc8a5ab696a6542454876c7b8ee17a365ce87b->enter($__internal_e2eb85607cad2b1c36168a8aaafc8a5ab696a6542454876c7b8ee17a365ce87b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_e2eb85607cad2b1c36168a8aaafc8a5ab696a6542454876c7b8ee17a365ce87b->leave($__internal_e2eb85607cad2b1c36168a8aaafc8a5ab696a6542454876c7b8ee17a365ce87b_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_cb1f60a091798ef7aa4e4ccd607a2d1c9e155c1ceb579fe481777a9d8de4f505 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cb1f60a091798ef7aa4e4ccd607a2d1c9e155c1ceb579fe481777a9d8de4f505->enter($__internal_cb1f60a091798ef7aa4e4ccd607a2d1c9e155c1ceb579fe481777a9d8de4f505_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_cb1f60a091798ef7aa4e4ccd607a2d1c9e155c1ceb579fe481777a9d8de4f505->leave($__internal_cb1f60a091798ef7aa4e4ccd607a2d1c9e155c1ceb579fe481777a9d8de4f505_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "WebProfilerBundle:Profiler:toolbar_redirect.html.twig", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/toolbar_redirect.html.twig");
    }
}
