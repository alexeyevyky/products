<?php

/* @Framework/Form/form_enctype.html.php */
class __TwigTemplate_558d7643b9d8ed5de7276f5f4a48c0ed991523abef4401bf9ddf12f898df89d7 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_7b94d866fcf0e30ffb6868976bd47a2eb0a2a02305da296853d944bcd266e778 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_7b94d866fcf0e30ffb6868976bd47a2eb0a2a02305da296853d944bcd266e778->enter($__internal_7b94d866fcf0e30ffb6868976bd47a2eb0a2a02305da296853d944bcd266e778_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/form_enctype.html.php"));

        // line 1
        echo "<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
";
        
        $__internal_7b94d866fcf0e30ffb6868976bd47a2eb0a2a02305da296853d944bcd266e778->leave($__internal_7b94d866fcf0e30ffb6868976bd47a2eb0a2a02305da296853d944bcd266e778_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/form_enctype.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php if (\$form->vars['multipart']): ?>enctype=\"multipart/form-data\"<?php endif ?>
", "@Framework/Form/form_enctype.html.php", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\form_enctype.html.php");
    }
}
