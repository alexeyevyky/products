<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_60b3e76cf4ca162428635332fd50e493043e0c11521441fd004d1bd0eea34e02 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ef2a649811c3a402d4c8e0e8ab1eba202ad2dbbe8df2c9bd35b1db97d8df5c9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef2a649811c3a402d4c8e0e8ab1eba202ad2dbbe8df2c9bd35b1db97d8df5c9b->enter($__internal_ef2a649811c3a402d4c8e0e8ab1eba202ad2dbbe8df2c9bd35b1db97d8df5c9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ef2a649811c3a402d4c8e0e8ab1eba202ad2dbbe8df2c9bd35b1db97d8df5c9b->leave($__internal_ef2a649811c3a402d4c8e0e8ab1eba202ad2dbbe8df2c9bd35b1db97d8df5c9b_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_d1f5ca916b5e32f8cb1f344b506250d2a9917f23a2e34615f4028b1652633cd5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d1f5ca916b5e32f8cb1f344b506250d2a9917f23a2e34615f4028b1652633cd5->enter($__internal_d1f5ca916b5e32f8cb1f344b506250d2a9917f23a2e34615f4028b1652633cd5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_d1f5ca916b5e32f8cb1f344b506250d2a9917f23a2e34615f4028b1652633cd5->leave($__internal_d1f5ca916b5e32f8cb1f344b506250d2a9917f23a2e34615f4028b1652633cd5_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_ceabc7928de91e4db75237ac55e4a3d68e71406a8ecd7c1be2583c1a595ae944 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ceabc7928de91e4db75237ac55e4a3d68e71406a8ecd7c1be2583c1a595ae944->enter($__internal_ceabc7928de91e4db75237ac55e4a3d68e71406a8ecd7c1be2583c1a595ae944_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_ceabc7928de91e4db75237ac55e4a3d68e71406a8ecd7c1be2583c1a595ae944->leave($__internal_ceabc7928de91e4db75237ac55e4a3d68e71406a8ecd7c1be2583c1a595ae944_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_6290e8750f5a1642e0d0b47d60a76dd2b73f72d6b18c091779cc0117aedadbaf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6290e8750f5a1642e0d0b47d60a76dd2b73f72d6b18c091779cc0117aedadbaf->enter($__internal_6290e8750f5a1642e0d0b47d60a76dd2b73f72d6b18c091779cc0117aedadbaf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_6290e8750f5a1642e0d0b47d60a76dd2b73f72d6b18c091779cc0117aedadbaf->leave($__internal_6290e8750f5a1642e0d0b47d60a76dd2b73f72d6b18c091779cc0117aedadbaf_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
