<?php

/* WebProfilerBundle:Collector:router.html.twig */
class __TwigTemplate_d8de6ef6206b3efe48fb8f520956540d94601901faf047c4bddba0614635ba8f extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "WebProfilerBundle:Collector:router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_cd2d8d7d16b1133138e3717a99757e4b0e7477dc37df4ab767aaa2ec4fcda4bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_cd2d8d7d16b1133138e3717a99757e4b0e7477dc37df4ab767aaa2ec4fcda4bb->enter($__internal_cd2d8d7d16b1133138e3717a99757e4b0e7477dc37df4ab767aaa2ec4fcda4bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Collector:router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_cd2d8d7d16b1133138e3717a99757e4b0e7477dc37df4ab767aaa2ec4fcda4bb->leave($__internal_cd2d8d7d16b1133138e3717a99757e4b0e7477dc37df4ab767aaa2ec4fcda4bb_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_40d5cc5313fd6076325a1382f0d22cfb95a1ad348159739530051df7fdc19cbf = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_40d5cc5313fd6076325a1382f0d22cfb95a1ad348159739530051df7fdc19cbf->enter($__internal_40d5cc5313fd6076325a1382f0d22cfb95a1ad348159739530051df7fdc19cbf_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_40d5cc5313fd6076325a1382f0d22cfb95a1ad348159739530051df7fdc19cbf->leave($__internal_40d5cc5313fd6076325a1382f0d22cfb95a1ad348159739530051df7fdc19cbf_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_5d1d5a7564529a3d5925921b3c8abe2ff8021022efc6d5aba85bd8b0ec69f259 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5d1d5a7564529a3d5925921b3c8abe2ff8021022efc6d5aba85bd8b0ec69f259->enter($__internal_5d1d5a7564529a3d5925921b3c8abe2ff8021022efc6d5aba85bd8b0ec69f259_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_5d1d5a7564529a3d5925921b3c8abe2ff8021022efc6d5aba85bd8b0ec69f259->leave($__internal_5d1d5a7564529a3d5925921b3c8abe2ff8021022efc6d5aba85bd8b0ec69f259_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_8bf800a47226d7fba2c04eac55d565c309d63b23454ddb938f1f3505e4a0e063 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8bf800a47226d7fba2c04eac55d565c309d63b23454ddb938f1f3505e4a0e063->enter($__internal_8bf800a47226d7fba2c04eac55d565c309d63b23454ddb938f1f3505e4a0e063_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_8bf800a47226d7fba2c04eac55d565c309d63b23454ddb938f1f3505e4a0e063->leave($__internal_8bf800a47226d7fba2c04eac55d565c309d63b23454ddb938f1f3505e4a0e063_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Collector:router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "WebProfilerBundle:Collector:router.html.twig", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Collector/router.html.twig");
    }
}
