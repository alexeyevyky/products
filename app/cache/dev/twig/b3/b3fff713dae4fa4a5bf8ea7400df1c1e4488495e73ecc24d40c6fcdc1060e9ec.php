<?php

/* TwigBundle:Exception:exception_full.html.twig */
class __TwigTemplate_ebe648d3ccfec40fd78e4b0ad907e4ecc8c19d20893dab56d8f79edbc9367666 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "TwigBundle:Exception:exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a3d7015839e04034c23e77b28d7c08ef86ac0f789b94a4110380e0258d158912 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a3d7015839e04034c23e77b28d7c08ef86ac0f789b94a4110380e0258d158912->enter($__internal_a3d7015839e04034c23e77b28d7c08ef86ac0f789b94a4110380e0258d158912_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "TwigBundle:Exception:exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a3d7015839e04034c23e77b28d7c08ef86ac0f789b94a4110380e0258d158912->leave($__internal_a3d7015839e04034c23e77b28d7c08ef86ac0f789b94a4110380e0258d158912_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_b33379f992f2271cec04541fafb1e34e0ab75ceb29f316f351de2bc31bc8ab47 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b33379f992f2271cec04541fafb1e34e0ab75ceb29f316f351de2bc31bc8ab47->enter($__internal_b33379f992f2271cec04541fafb1e34e0ab75ceb29f316f351de2bc31bc8ab47_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_b33379f992f2271cec04541fafb1e34e0ab75ceb29f316f351de2bc31bc8ab47->leave($__internal_b33379f992f2271cec04541fafb1e34e0ab75ceb29f316f351de2bc31bc8ab47_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_e7404ade7857ff74c1020724f070622523f9992a349fd5c0e955abcd7a896c53 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e7404ade7857ff74c1020724f070622523f9992a349fd5c0e955abcd7a896c53->enter($__internal_e7404ade7857ff74c1020724f070622523f9992a349fd5c0e955abcd7a896c53_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_e7404ade7857ff74c1020724f070622523f9992a349fd5c0e955abcd7a896c53->leave($__internal_e7404ade7857ff74c1020724f070622523f9992a349fd5c0e955abcd7a896c53_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_e8fdca958ee37e2071cdace5a65c0f369b9218f154aecfaeda2b6a1617544807 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e8fdca958ee37e2071cdace5a65c0f369b9218f154aecfaeda2b6a1617544807->enter($__internal_e8fdca958ee37e2071cdace5a65c0f369b9218f154aecfaeda2b6a1617544807_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "TwigBundle:Exception:exception_full.html.twig", 12)->display($context);
        
        $__internal_e8fdca958ee37e2071cdace5a65c0f369b9218f154aecfaeda2b6a1617544807->leave($__internal_e8fdca958ee37e2071cdace5a65c0f369b9218f154aecfaeda2b6a1617544807_prof);

    }

    public function getTemplateName()
    {
        return "TwigBundle:Exception:exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "TwigBundle:Exception:exception_full.html.twig", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle/Resources/views/Exception/exception_full.html.twig");
    }
}
