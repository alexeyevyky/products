<?php

/* :default:index.html.twig */
class __TwigTemplate_61ec16f18dd9df2db5af2e33e7f6df7cc681c3476dace56cf1218c964cfad580 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":default:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_867aab8ce7809384469568b26a93362fa2a819c85054d9d69290c610c7982f8c = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_867aab8ce7809384469568b26a93362fa2a819c85054d9d69290c610c7982f8c->enter($__internal_867aab8ce7809384469568b26a93362fa2a819c85054d9d69290c610c7982f8c_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":default:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_867aab8ce7809384469568b26a93362fa2a819c85054d9d69290c610c7982f8c->leave($__internal_867aab8ce7809384469568b26a93362fa2a819c85054d9d69290c610c7982f8c_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_2958d2e98994c011ee1259a8e40811dec38e20d3e54f05a5cfcbc81a90326c0f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2958d2e98994c011ee1259a8e40811dec38e20d3e54f05a5cfcbc81a90326c0f->enter($__internal_2958d2e98994c011ee1259a8e40811dec38e20d3e54f05a5cfcbc81a90326c0f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <div class=\"container\">
        <nav class=\"navbar navbar-default\">
            <div class=\"container-fluid\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a class=\"navbar-brand\" href=\"#\">VE</a>
                </div>
                <div id=\"navbar\" class=\"navbar-collapse collapse\">
                    <ul class=\"nav navbar-nav\">
                        <li class=\"active\"><a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_index");
        echo "\">Categories</a></li>
                        <li><a href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_index");
        echo "\">Products</a></li>             
                    </ul>           
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

        <h1>PHP Practical Test</h1>
 
";
        
        $__internal_2958d2e98994c011ee1259a8e40811dec38e20d3e54f05a5cfcbc81a90326c0f->leave($__internal_2958d2e98994c011ee1259a8e40811dec38e20d3e54f05a5cfcbc81a90326c0f_prof);

    }

    public function getTemplateName()
    {
        return ":default:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  64 => 20,  60 => 19,  56 => 18,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <div class=\"container\">
        <nav class=\"navbar navbar-default\">
            <div class=\"container-fluid\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a class=\"navbar-brand\" href=\"#\">VE</a>
                </div>
                <div id=\"navbar\" class=\"navbar-collapse collapse\">
                    <ul class=\"nav navbar-nav\">
                        <li class=\"active\"><a href=\"{{path('homepage')}}\">Home</a></li>
                        <li><a href=\"{{path('categories_index')}}\">Categories</a></li>
                        <li><a href=\"{{path('products_index')}}\">Products</a></li>             
                    </ul>           
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

        <h1>PHP Practical Test</h1>
 
{% endblock %}", ":default:index.html.twig", "C:\\xampp\\htdocs\\products\\app/Resources\\views/default/index.html.twig");
    }
}
