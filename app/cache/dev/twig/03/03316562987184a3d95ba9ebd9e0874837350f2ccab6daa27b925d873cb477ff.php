<?php

/* @WebProfiler/Profiler/toolbar_redirect.html.twig */
class __TwigTemplate_f7420b44c1e192e093c79e9832446d7d76797d8149ac36460d3bcc1726bc9bba extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@WebProfiler/Profiler/toolbar_redirect.html.twig", 1);
        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_e4302a4ee5d60c7efb00e3fedae4dcde11370e4357dbf310a419173506d55f4a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e4302a4ee5d60c7efb00e3fedae4dcde11370e4357dbf310a419173506d55f4a->enter($__internal_e4302a4ee5d60c7efb00e3fedae4dcde11370e4357dbf310a419173506d55f4a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Profiler/toolbar_redirect.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_e4302a4ee5d60c7efb00e3fedae4dcde11370e4357dbf310a419173506d55f4a->leave($__internal_e4302a4ee5d60c7efb00e3fedae4dcde11370e4357dbf310a419173506d55f4a_prof);

    }

    // line 3
    public function block_title($context, array $blocks = array())
    {
        $__internal_ef42a81d3000be725041583a280cc4deb6e6387f850d56a22597326050942fa2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ef42a81d3000be725041583a280cc4deb6e6387f850d56a22597326050942fa2->enter($__internal_ef42a81d3000be725041583a280cc4deb6e6387f850d56a22597326050942fa2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Redirection Intercepted";
        
        $__internal_ef42a81d3000be725041583a280cc4deb6e6387f850d56a22597326050942fa2->leave($__internal_ef42a81d3000be725041583a280cc4deb6e6387f850d56a22597326050942fa2_prof);

    }

    // line 5
    public function block_body($context, array $blocks = array())
    {
        $__internal_6f0bb4321119a3419305e289f0a6377f7844bccbca4217c94e0478b056255f06 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6f0bb4321119a3419305e289f0a6377f7844bccbca4217c94e0478b056255f06->enter($__internal_6f0bb4321119a3419305e289f0a6377f7844bccbca4217c94e0478b056255f06_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"";
        // line 8
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "\">";
        echo twig_escape_filter($this->env, (isset($context["location"]) ? $context["location"] : $this->getContext($context, "location")), "html", null, true);
        echo "</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
";
        
        $__internal_6f0bb4321119a3419305e289f0a6377f7844bccbca4217c94e0478b056255f06->leave($__internal_6f0bb4321119a3419305e289f0a6377f7844bccbca4217c94e0478b056255f06_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Profiler/toolbar_redirect.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  57 => 8,  53 => 6,  47 => 5,  35 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block title 'Redirection Intercepted' %}

{% block body %}
    <div class=\"sf-reset\">
        <div class=\"block-exception\">
            <h1>This request redirects to <a href=\"{{ location }}\">{{ location }}</a>.</h1>

            <p>
                <small>
                    The redirect was intercepted by the web debug toolbar to help debugging.
                    For more information, see the \"intercept-redirects\" option of the Profiler.
                </small>
            </p>
        </div>
    </div>
{% endblock %}
", "@WebProfiler/Profiler/toolbar_redirect.html.twig", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Profiler\\toolbar_redirect.html.twig");
    }
}
