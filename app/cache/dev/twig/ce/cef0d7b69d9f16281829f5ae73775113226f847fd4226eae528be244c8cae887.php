<?php

/* @Twig/Exception/exception_full.html.twig */
class __TwigTemplate_2ec2821dacbf58ad408add18c75ebf724023c50aba0683b9de785e26711ac144 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@Twig/layout.html.twig", "@Twig/Exception/exception_full.html.twig", 1);
        $this->blocks = array(
            'head' => array($this, 'block_head'),
            'title' => array($this, 'block_title'),
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@Twig/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_ea97d91956c191f5e02ded0074728b6b6369911894b4a34196b1e0b9849b03dd = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_ea97d91956c191f5e02ded0074728b6b6369911894b4a34196b1e0b9849b03dd->enter($__internal_ea97d91956c191f5e02ded0074728b6b6369911894b4a34196b1e0b9849b03dd_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Twig/Exception/exception_full.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_ea97d91956c191f5e02ded0074728b6b6369911894b4a34196b1e0b9849b03dd->leave($__internal_ea97d91956c191f5e02ded0074728b6b6369911894b4a34196b1e0b9849b03dd_prof);

    }

    // line 3
    public function block_head($context, array $blocks = array())
    {
        $__internal_d15ec51d8d09920d462642a94d13c1ce99927afb5a2cad5c7ceed5294f71328a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d15ec51d8d09920d462642a94d13c1ce99927afb5a2cad5c7ceed5294f71328a->enter($__internal_d15ec51d8d09920d462642a94d13c1ce99927afb5a2cad5c7ceed5294f71328a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "head"));

        // line 4
        echo "    <link href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpFoundationExtension')->generateAbsoluteUrl($this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/framework/css/exception.css")), "html", null, true);
        echo "\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
";
        
        $__internal_d15ec51d8d09920d462642a94d13c1ce99927afb5a2cad5c7ceed5294f71328a->leave($__internal_d15ec51d8d09920d462642a94d13c1ce99927afb5a2cad5c7ceed5294f71328a_prof);

    }

    // line 7
    public function block_title($context, array $blocks = array())
    {
        $__internal_0764833701b2e2dffbbf1c24048016ab9093cb34dff36f3b474090558a9de91e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0764833701b2e2dffbbf1c24048016ab9093cb34dff36f3b474090558a9de91e->enter($__internal_0764833701b2e2dffbbf1c24048016ab9093cb34dff36f3b474090558a9de91e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        // line 8
        echo "    ";
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["exception"]) ? $context["exception"] : $this->getContext($context, "exception")), "message", array()), "html", null, true);
        echo " (";
        echo twig_escape_filter($this->env, (isset($context["status_code"]) ? $context["status_code"] : $this->getContext($context, "status_code")), "html", null, true);
        echo " ";
        echo twig_escape_filter($this->env, (isset($context["status_text"]) ? $context["status_text"] : $this->getContext($context, "status_text")), "html", null, true);
        echo ")
";
        
        $__internal_0764833701b2e2dffbbf1c24048016ab9093cb34dff36f3b474090558a9de91e->leave($__internal_0764833701b2e2dffbbf1c24048016ab9093cb34dff36f3b474090558a9de91e_prof);

    }

    // line 11
    public function block_body($context, array $blocks = array())
    {
        $__internal_8c78b0d27f4ba22801ad3f1d7b21b2e9beedd64d58e64a303e1c0cb165ea74bb = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8c78b0d27f4ba22801ad3f1d7b21b2e9beedd64d58e64a303e1c0cb165ea74bb->enter($__internal_8c78b0d27f4ba22801ad3f1d7b21b2e9beedd64d58e64a303e1c0cb165ea74bb_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 12
        echo "    ";
        $this->loadTemplate("@Twig/Exception/exception.html.twig", "@Twig/Exception/exception_full.html.twig", 12)->display($context);
        
        $__internal_8c78b0d27f4ba22801ad3f1d7b21b2e9beedd64d58e64a303e1c0cb165ea74bb->leave($__internal_8c78b0d27f4ba22801ad3f1d7b21b2e9beedd64d58e64a303e1c0cb165ea74bb_prof);

    }

    public function getTemplateName()
    {
        return "@Twig/Exception/exception_full.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  78 => 12,  72 => 11,  58 => 8,  52 => 7,  42 => 4,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@Twig/layout.html.twig' %}

{% block head %}
    <link href=\"{{ absolute_url(asset('bundles/framework/css/exception.css')) }}\" rel=\"stylesheet\" type=\"text/css\" media=\"all\" />
{% endblock %}

{% block title %}
    {{ exception.message }} ({{ status_code }} {{ status_text }})
{% endblock %}

{% block body %}
    {% include '@Twig/Exception/exception.html.twig' %}
{% endblock %}
", "@Twig/Exception/exception_full.html.twig", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\TwigBundle\\Resources\\views\\Exception\\exception_full.html.twig");
    }
}
