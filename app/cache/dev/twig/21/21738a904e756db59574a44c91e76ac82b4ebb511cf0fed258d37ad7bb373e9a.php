<?php

/* base.html.twig */
class __TwigTemplate_26b29e82a994b82222eba6c14222b4fd59ea239cdb4223430dec5a069f8b7d44 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_88dba739ed54755c6f15bb48a0437541b644c92b6060f9ec6dc559e06c17a6c3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_88dba739ed54755c6f15bb48a0437541b644c92b6060f9ec6dc559e06c17a6c3->enter($__internal_88dba739ed54755c6f15bb48a0437541b644c92b6060f9ec6dc559e06c17a6c3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
    ";
        // line 14
        $this->displayBlock('body', $context, $blocks);
        // line 15
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 20
        echo "</body>
</html>
";
        
        $__internal_88dba739ed54755c6f15bb48a0437541b644c92b6060f9ec6dc559e06c17a6c3->leave($__internal_88dba739ed54755c6f15bb48a0437541b644c92b6060f9ec6dc559e06c17a6c3_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_b2a8873f536e0a563fdfd8af81bde923db82b3e23663f34c9148b49cda09e72e = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b2a8873f536e0a563fdfd8af81bde923db82b3e23663f34c9148b49cda09e72e->enter($__internal_b2a8873f536e0a563fdfd8af81bde923db82b3e23663f34c9148b49cda09e72e_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_b2a8873f536e0a563fdfd8af81bde923db82b3e23663f34c9148b49cda09e72e->leave($__internal_b2a8873f536e0a563fdfd8af81bde923db82b3e23663f34c9148b49cda09e72e_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_64148c6f4a054258b9821490777700dd880cac85d65ba7ba7db6ae63a14576a0 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64148c6f4a054258b9821490777700dd880cac85d65ba7ba7db6ae63a14576a0->enter($__internal_64148c6f4a054258b9821490777700dd880cac85d65ba7ba7db6ae63a14576a0_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/css/style.css"), "html", null, true);
        echo "\" />
            
        ";
        
        $__internal_64148c6f4a054258b9821490777700dd880cac85d65ba7ba7db6ae63a14576a0->leave($__internal_64148c6f4a054258b9821490777700dd880cac85d65ba7ba7db6ae63a14576a0_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_379a5ed260bcadb0bf14ba57a6ef146b8251be368fad517c52b14bdf9abd8813 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_379a5ed260bcadb0bf14ba57a6ef146b8251be368fad517c52b14bdf9abd8813->enter($__internal_379a5ed260bcadb0bf14ba57a6ef146b8251be368fad517c52b14bdf9abd8813_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_379a5ed260bcadb0bf14ba57a6ef146b8251be368fad517c52b14bdf9abd8813->leave($__internal_379a5ed260bcadb0bf14ba57a6ef146b8251be368fad517c52b14bdf9abd8813_prof);

    }

    // line 15
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_e831afd639c91c3795f7216b420cb9c1b9e4225bff2c0b00a5ac7f0b9ce13ef3 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e831afd639c91c3795f7216b420cb9c1b9e4225bff2c0b00a5ac7f0b9ce13ef3->enter($__internal_e831afd639c91c3795f7216b420cb9c1b9e4225bff2c0b00a5ac7f0b9ce13ef3_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 16
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/tether.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    ";
        
        $__internal_e831afd639c91c3795f7216b420cb9c1b9e4225bff2c0b00a5ac7f0b9ce13ef3->leave($__internal_e831afd639c91c3795f7216b420cb9c1b9e4225bff2c0b00a5ac7f0b9ce13ef3_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 18,  114 => 17,  109 => 16,  103 => 15,  92 => 14,  82 => 8,  77 => 7,  71 => 6,  59 => 5,  50 => 20,  47 => 15,  45 => 14,  38 => 11,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/categories/css/bootstrap.min.css') }}\" />
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/categories/css/style.css') }}\" />
            
        {% endblock  %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
    {% block body %}{% endblock %}
    {% block javascripts %}
        <script src=\"{{ asset('bundles/categories/js/jquery.min.js') }}\"></script>
        <script src=\"{{ asset('bundles/categories/js/tether.min.js') }}\"></script>
        <script src=\"{{ asset('bundles/categories/js/bootstrap.min.js') }}\"></script>
    {% endblock  %}
</body>
</html>
", "base.html.twig", "C:\\xampp\\htdocs\\products\\app\\Resources\\views\\base.html.twig");
    }
}
