<?php

/* @Framework/Form/hidden_widget.html.php */
class __TwigTemplate_0d40413497ad6816e8619b8c54640c4308262acf059a4cc7e00ea05c91f76831 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_fb44d8c29eb0d57d87a0d86ed64e5bd7a54ac3aad98027056ae63c5ce54d6371 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fb44d8c29eb0d57d87a0d86ed64e5bd7a54ac3aad98027056ae63c5ce54d6371->enter($__internal_fb44d8c29eb0d57d87a0d86ed64e5bd7a54ac3aad98027056ae63c5ce54d6371_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/hidden_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
";
        
        $__internal_fb44d8c29eb0d57d87a0d86ed64e5bd7a54ac3aad98027056ae63c5ce54d6371->leave($__internal_fb44d8c29eb0d57d87a0d86ed64e5bd7a54ac3aad98027056ae63c5ce54d6371_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/hidden_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'hidden')) ?>
", "@Framework/Form/hidden_widget.html.php", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\hidden_widget.html.php");
    }
}
