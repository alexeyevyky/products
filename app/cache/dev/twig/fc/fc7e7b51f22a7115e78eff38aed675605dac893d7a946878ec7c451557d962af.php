<?php

/* base.html.twig */
class __TwigTemplate_253c43d3d5ef588cc40f65d19118330e85efae99cbcb55ad10c4d23cf27ab24a extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_4c066a24affbfd7057b7f7feb09464f7658a823abde181294c4c4e7161dd0d52 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4c066a24affbfd7057b7f7feb09464f7658a823abde181294c4c4e7161dd0d52->enter($__internal_4c066a24affbfd7057b7f7feb09464f7658a823abde181294c4c4e7161dd0d52_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
    ";
        // line 14
        $this->displayBlock('body', $context, $blocks);
        // line 15
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 20
        echo "</body>
</html>
";
        
        $__internal_4c066a24affbfd7057b7f7feb09464f7658a823abde181294c4c4e7161dd0d52->leave($__internal_4c066a24affbfd7057b7f7feb09464f7658a823abde181294c4c4e7161dd0d52_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_e72383a53a326458f438769ec512323e8a92f4589098e7349c40d0baa410db6f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_e72383a53a326458f438769ec512323e8a92f4589098e7349c40d0baa410db6f->enter($__internal_e72383a53a326458f438769ec512323e8a92f4589098e7349c40d0baa410db6f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_e72383a53a326458f438769ec512323e8a92f4589098e7349c40d0baa410db6f->leave($__internal_e72383a53a326458f438769ec512323e8a92f4589098e7349c40d0baa410db6f_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_a22e7f8f7d28674849a770fb6f4845cafd6a896e4cb5e0ef08373d55fda9d732 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a22e7f8f7d28674849a770fb6f4845cafd6a896e4cb5e0ef08373d55fda9d732->enter($__internal_a22e7f8f7d28674849a770fb6f4845cafd6a896e4cb5e0ef08373d55fda9d732_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/css/style.css"), "html", null, true);
        echo "\" />
            
        ";
        
        $__internal_a22e7f8f7d28674849a770fb6f4845cafd6a896e4cb5e0ef08373d55fda9d732->leave($__internal_a22e7f8f7d28674849a770fb6f4845cafd6a896e4cb5e0ef08373d55fda9d732_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_b112c4533c3b0b52d2668862c42ba1ff510cd9dae64e2e06e1b90919c8e5ed77 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b112c4533c3b0b52d2668862c42ba1ff510cd9dae64e2e06e1b90919c8e5ed77->enter($__internal_b112c4533c3b0b52d2668862c42ba1ff510cd9dae64e2e06e1b90919c8e5ed77_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_b112c4533c3b0b52d2668862c42ba1ff510cd9dae64e2e06e1b90919c8e5ed77->leave($__internal_b112c4533c3b0b52d2668862c42ba1ff510cd9dae64e2e06e1b90919c8e5ed77_prof);

    }

    // line 15
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_f10a2550a695d7779cb3bc078c1619901cca3c5be49dd28c43ee91212cb55e1f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f10a2550a695d7779cb3bc078c1619901cca3c5be49dd28c43ee91212cb55e1f->enter($__internal_f10a2550a695d7779cb3bc078c1619901cca3c5be49dd28c43ee91212cb55e1f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 16
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/tether.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    ";
        
        $__internal_f10a2550a695d7779cb3bc078c1619901cca3c5be49dd28c43ee91212cb55e1f->leave($__internal_f10a2550a695d7779cb3bc078c1619901cca3c5be49dd28c43ee91212cb55e1f_prof);

    }

    public function getTemplateName()
    {
        return "base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 18,  114 => 17,  109 => 16,  103 => 15,  92 => 14,  82 => 8,  77 => 7,  71 => 6,  59 => 5,  50 => 20,  47 => 15,  45 => 14,  38 => 11,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/categories/css/bootstrap.min.css') }}\" />
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/categories/css/style.css') }}\" />
            
        {% endblock  %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
    {% block body %}{% endblock %}
    {% block javascripts %}
        <script src=\"{{ asset('bundles/categories/js/jquery.min.js') }}\"></script>
        <script src=\"{{ asset('bundles/categories/js/tether.min.js') }}\"></script>
        <script src=\"{{ asset('bundles/categories/js/bootstrap.min.js') }}\"></script>
    {% endblock  %}
</body>
</html>
", "base.html.twig", "C:\\xampp\\htdocs\\products\\app\\Resources\\views\\base.html.twig");
    }
}
