<?php

/* @Categories/categories/index.html.twig */
class __TwigTemplate_5cd71a0903ed5dcdecb4e95a8f3059e3f5cef728c126493b7b75e4348b75527e extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "@Categories/categories/index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9fd738fe933b04b2158a6205af929f2f3bba5e26c560b0e2b8eb23b0c3f75f75 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9fd738fe933b04b2158a6205af929f2f3bba5e26c560b0e2b8eb23b0c3f75f75->enter($__internal_9fd738fe933b04b2158a6205af929f2f3bba5e26c560b0e2b8eb23b0c3f75f75_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Categories/categories/index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_9fd738fe933b04b2158a6205af929f2f3bba5e26c560b0e2b8eb23b0c3f75f75->leave($__internal_9fd738fe933b04b2158a6205af929f2f3bba5e26c560b0e2b8eb23b0c3f75f75_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_c832f1715d158521bd4a6deabc6f8f6d4bd5683fae711c66eadb6805a51e86f8 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_c832f1715d158521bd4a6deabc6f8f6d4bd5683fae711c66eadb6805a51e86f8->enter($__internal_c832f1715d158521bd4a6deabc6f8f6d4bd5683fae711c66eadb6805a51e86f8_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "
    <div class=\"container\">
        <nav class=\"navbar navbar-default\">
            <div class=\"container-fluid\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a class=\"navbar-brand\" href=\"#\">VE</a>
                </div>
                <div id=\"navbar\" class=\"navbar-collapse collapse\">
                    <ul class=\"nav navbar-nav\">
                        <li><a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Home</a></li>
                        <li class=\"active\"><a href=\"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_index");
        echo "\">Categories</a></li>
                        <li><a href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_index");
        echo "\">Products</a></li>             
                    </ul>           
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

        <h1>Categories list</h1>

        <div class=\"table-responsive\">
            <table class=\"table table-hover\">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Category</th>
                        <th>Updated at</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <button type=\"button\" class=\"btn btn-primary btn-lg\" data-toggle=\"modal\" data-target=\"#categoryModal\">
            Create a new category
        </button>    
    </div>
    <!-- Modal -->
    <div class=\"modal fade\" id=\"categoryModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Category</h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"alerts\"></div>
                    <form class=\"form-row\">
                        <input type=\"hidden\" name=\"id\" id=\"category_id\"/>
                        <input type=\"hidden\" name=\"updatedAt\" id=\"updatedAt\" value=\"";
        // line 61
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y-m-d H:i:s"), "html", null, true);
        echo "\"/>
                        <div class=\"form-group\"> 
                            <label for=\"category\">Category name</label> 
                            <input type=\"text\" class=\"form-control\" id=\"category_name\" name=\"category\" placeholder=\"Category name...\"> 
                        </div>
                    </form>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <button type=\"button\" class=\"btn btn-primary\" onclick=\"Categories.addEdit()\">Save changes</button>
                </div>
            </div>
        </div>
    </div>      
";
        
        $__internal_c832f1715d158521bd4a6deabc6f8f6d4bd5683fae711c66eadb6805a51e86f8->leave($__internal_c832f1715d158521bd4a6deabc6f8f6d4bd5683fae711c66eadb6805a51e86f8_prof);

    }

    // line 77
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_8844cd31aef1e8705dd469766a069a7e865b49ea42170bab890f5b555db5796d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_8844cd31aef1e8705dd469766a069a7e865b49ea42170bab890f5b555db5796d->enter($__internal_8844cd31aef1e8705dd469766a069a7e865b49ea42170bab890f5b555db5796d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 78
        echo "    <script type=\"text/javascript\">
        var _add_category = '";
        // line 79
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_new");
        echo "';
        var _load_category = '";
        // line 80
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_load");
        echo "';
        var _delete_category = '";
        // line 81
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_remove", array("id" => 0));
        echo "'
       
    </script>
    <script src=\"";
        // line 84
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 85
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/tether.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 86
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>s
    <script src=\"";
        // line 87
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/functions.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_8844cd31aef1e8705dd469766a069a7e865b49ea42170bab890f5b555db5796d->leave($__internal_8844cd31aef1e8705dd469766a069a7e865b49ea42170bab890f5b555db5796d_prof);

    }

    public function getTemplateName()
    {
        return "@Categories/categories/index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  167 => 87,  163 => 86,  159 => 85,  155 => 84,  149 => 81,  145 => 80,  141 => 79,  138 => 78,  132 => 77,  110 => 61,  66 => 20,  62 => 19,  58 => 18,  41 => 3,  35 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block body %}

    <div class=\"container\">
        <nav class=\"navbar navbar-default\">
            <div class=\"container-fluid\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a class=\"navbar-brand\" href=\"#\">VE</a>
                </div>
                <div id=\"navbar\" class=\"navbar-collapse collapse\">
                    <ul class=\"nav navbar-nav\">
                        <li><a href=\"{{path('homepage')}}\">Home</a></li>
                        <li class=\"active\"><a href=\"{{path('categories_index')}}\">Categories</a></li>
                        <li><a href=\"{{path('products_index')}}\">Products</a></li>             
                    </ul>           
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

        <h1>Categories list</h1>

        <div class=\"table-responsive\">
            <table class=\"table table-hover\">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Category</th>
                        <th>Updated at</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <button type=\"button\" class=\"btn btn-primary btn-lg\" data-toggle=\"modal\" data-target=\"#categoryModal\">
            Create a new category
        </button>    
    </div>
    <!-- Modal -->
    <div class=\"modal fade\" id=\"categoryModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Category</h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"alerts\"></div>
                    <form class=\"form-row\">
                        <input type=\"hidden\" name=\"id\" id=\"category_id\"/>
                        <input type=\"hidden\" name=\"updatedAt\" id=\"updatedAt\" value=\"{{\"now\"|date(\"Y-m-d H:i:s\")}}\"/>
                        <div class=\"form-group\"> 
                            <label for=\"category\">Category name</label> 
                            <input type=\"text\" class=\"form-control\" id=\"category_name\" name=\"category\" placeholder=\"Category name...\"> 
                        </div>
                    </form>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <button type=\"button\" class=\"btn btn-primary\" onclick=\"Categories.addEdit()\">Save changes</button>
                </div>
            </div>
        </div>
    </div>      
{% endblock %}

{% block javascripts %}
    <script type=\"text/javascript\">
        var _add_category = '{{path('categories_new')}}';
        var _load_category = '{{path('categories_load')}}';
        var _delete_category = '{{path('categories_remove', { 'id': 0}) }}'
       
    </script>
    <script src=\"{{ asset('bundles/categories/js/jquery.min.js') }}\"></script>
    <script src=\"{{ asset('bundles/categories/js/tether.min.js') }}\"></script>
    <script src=\"{{ asset('bundles/categories/js/bootstrap.min.js') }}\"></script>s
    <script src=\"{{ asset('bundles/categories/js/functions.js') }}\"></script>
{% endblock  %}

", "@Categories/categories/index.html.twig", "C:\\xampp\\htdocs\\products\\src\\CategoriesBundle\\Resources\\views\\categories\\index.html.twig");
    }
}
