<?php

/* ProductsBundle:products:index.html.twig */
class __TwigTemplate_e5d1395fc2417edcab5c25d1877667f7c4489138023b985972a8a456c805b644 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", "ProductsBundle:products:index.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_64fbeba210674a3f0ae72a494094b9bf96bc8823d0e1ce655b804526cadf3868 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_64fbeba210674a3f0ae72a494094b9bf96bc8823d0e1ce655b804526cadf3868->enter($__internal_64fbeba210674a3f0ae72a494094b9bf96bc8823d0e1ce655b804526cadf3868_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "ProductsBundle:products:index.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_64fbeba210674a3f0ae72a494094b9bf96bc8823d0e1ce655b804526cadf3868->leave($__internal_64fbeba210674a3f0ae72a494094b9bf96bc8823d0e1ce655b804526cadf3868_prof);

    }

    // line 2
    public function block_body($context, array $blocks = array())
    {
        $__internal_b9167778b44262d02e183ca754a4e35d6d19e68345797334a93179e63c2011f7 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_b9167778b44262d02e183ca754a4e35d6d19e68345797334a93179e63c2011f7->enter($__internal_b9167778b44262d02e183ca754a4e35d6d19e68345797334a93179e63c2011f7_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 3
        echo "
    <div class=\"container\">
        <nav class=\"navbar navbar-default\">
            <div class=\"container-fluid\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a class=\"navbar-brand\" href=\"#\">VE</a>
                </div>
                <div id=\"navbar\" class=\"navbar-collapse collapse\">
                    <ul class=\"nav navbar-nav\">
                        <li><a href=\"";
        // line 18
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("homepage");
        echo "\">Home</a></li>
                        <li><a href=\"";
        // line 19
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("categories_index");
        echo "\">Categories</a></li>
                        <li class=\"active\"><a href=\"";
        // line 20
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_index");
        echo "\">Products</a></li>             
                    </ul>           
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

        <h1>Products list</h1>

        <div class=\"table-responsive\">
            <table class=\"table table-hover\">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Category</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Updated at</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <button type=\"button\" class=\"btn btn-primary btn-lg\" data-toggle=\"modal\" data-target=\"#productModal\">
            Create a new product
        </button>    
    </div>
    <!-- Modal -->
    <div class=\"modal fade\" id=\"productModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Product</h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"alerts\"></div>
                    <form class=\"form-row\">
                        <input type=\"hidden\" name=\"id\" id=\"product_id\"/>
                        <input type=\"hidden\" name=\"updatedAt\" id=\"updatedAt\" value=\"";
        // line 63
        echo twig_escape_filter($this->env, twig_date_format_filter($this->env, "now", "Y-m-d H:i:s"), "html", null, true);
        echo "\"/>
                        <div class=\"form-group\"> 
                            <label for=\"category\">Category</label> 
                            <select name=\"category_id\" id=\"category_id\" class=\"form-control\" required=\"required\">
                                <option value=\"\">-Choose-</option>
                                ";
        // line 68
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable((isset($context["categories"]) ? $context["categories"] : $this->getContext($context, "categories")));
        foreach ($context['_seq'] as $context["_key"] => $context["category"]) {
            // line 69
            echo "                                    <option value=\"";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "id", array()), "html", null, true);
            echo "\">";
            echo twig_escape_filter($this->env, $this->getAttribute($context["category"], "category", array()), "html", null, true);
            echo "</option>
                                ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['category'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 70
        echo "    
                            </select>
                        </div>
                        <div class=\"form-group\"> 
                            <label for=\"category\">Product name</label> 
                            <input type=\"text\" class=\"form-control\" required=\"required\" id=\"product_name\" name=\"product\" placeholder=\"Product name...\"> 
                        </div>
                        <div class=\"form-group\"> 
                            <label for=\"category\">Product price</label> 
                            <input type=\"number\" class=\"form-control\" required=\"required\" id=\"product_price\" name=\"price\" placeholder=\"Product price...\"> 
                        </div>
                    </form>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <button type=\"button\" class=\"btn btn-primary\" onclick=\"Products.addEdit()\">Save changes</button>
                </div>
            </div>
        </div>
    </div>      
";
        
        $__internal_b9167778b44262d02e183ca754a4e35d6d19e68345797334a93179e63c2011f7->leave($__internal_b9167778b44262d02e183ca754a4e35d6d19e68345797334a93179e63c2011f7_prof);

    }

    // line 92
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_3b668a4847c349187337c0226aa4a934e9c34d93b3df6c64c6a2d1d59de44979 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3b668a4847c349187337c0226aa4a934e9c34d93b3df6c64c6a2d1d59de44979->enter($__internal_3b668a4847c349187337c0226aa4a934e9c34d93b3df6c64c6a2d1d59de44979_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 93
        echo "    <script type=\"text/javascript\">
        var _add_product = '";
        // line 94
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_new");
        echo "';
        var _load_product = '";
        // line 95
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_load");
        echo "';
        var _delete_product = '";
        // line 96
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_remove", array("id" => 0));
        echo "'
      
    </script>
    <script src=\"";
        // line 99
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 100
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/tether.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 101
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    <script src=\"";
        // line 102
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/products/js/functions.js"), "html", null, true);
        echo "\"></script>
";
        
        $__internal_3b668a4847c349187337c0226aa4a934e9c34d93b3df6c64c6a2d1d59de44979->leave($__internal_3b668a4847c349187337c0226aa4a934e9c34d93b3df6c64c6a2d1d59de44979_prof);

    }

    public function getTemplateName()
    {
        return "ProductsBundle:products:index.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  197 => 102,  193 => 101,  189 => 100,  185 => 99,  179 => 96,  175 => 95,  171 => 94,  168 => 93,  162 => 92,  135 => 70,  124 => 69,  120 => 68,  112 => 63,  66 => 20,  62 => 19,  58 => 18,  41 => 3,  35 => 2,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}
{% block body %}

    <div class=\"container\">
        <nav class=\"navbar navbar-default\">
            <div class=\"container-fluid\">
                <div class=\"navbar-header\">
                    <button type=\"button\" class=\"navbar-toggle collapsed\" data-toggle=\"collapse\" data-target=\"#navbar\" aria-expanded=\"false\" aria-controls=\"navbar\">
                        <span class=\"sr-only\">Toggle navigation</span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                        <span class=\"icon-bar\"></span>
                    </button>
                    <a class=\"navbar-brand\" href=\"#\">VE</a>
                </div>
                <div id=\"navbar\" class=\"navbar-collapse collapse\">
                    <ul class=\"nav navbar-nav\">
                        <li><a href=\"{{path('homepage')}}\">Home</a></li>
                        <li><a href=\"{{path('categories_index')}}\">Categories</a></li>
                        <li class=\"active\"><a href=\"{{path('products_index')}}\">Products</a></li>             
                    </ul>           
                </div><!--/.nav-collapse -->
            </div><!--/.container-fluid -->
        </nav>

        <h1>Products list</h1>

        <div class=\"table-responsive\">
            <table class=\"table table-hover\">
                <thead>
                    <tr>
                        <th>Id</th>
                        <th>Category</th>
                        <th>Product</th>
                        <th>Price</th>
                        <th>Updated at</th>
                        <th>Actions</th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <button type=\"button\" class=\"btn btn-primary btn-lg\" data-toggle=\"modal\" data-target=\"#productModal\">
            Create a new product
        </button>    
    </div>
    <!-- Modal -->
    <div class=\"modal fade\" id=\"productModal\" tabindex=\"-1\" role=\"dialog\" aria-labelledby=\"myModalLabel\" aria-hidden=\"true\">
        <div class=\"modal-dialog\" role=\"document\">
            <div class=\"modal-content\">
                <div class=\"modal-header\">
                    <button type=\"button\" class=\"close\" data-dismiss=\"modal\" aria-label=\"Close\">
                        <span aria-hidden=\"true\">&times;</span>
                    </button>
                    <h4 class=\"modal-title\" id=\"myModalLabel\">Product</h4>
                </div>
                <div class=\"modal-body\">
                    <div id=\"alerts\"></div>
                    <form class=\"form-row\">
                        <input type=\"hidden\" name=\"id\" id=\"product_id\"/>
                        <input type=\"hidden\" name=\"updatedAt\" id=\"updatedAt\" value=\"{{\"now\"|date(\"Y-m-d H:i:s\")}}\"/>
                        <div class=\"form-group\"> 
                            <label for=\"category\">Category</label> 
                            <select name=\"category_id\" id=\"category_id\" class=\"form-control\" required=\"required\">
                                <option value=\"\">-Choose-</option>
                                {% for category in categories %}
                                    <option value=\"{{ category.id }}\">{{ category.category }}</option>
                                {% endfor %}    
                            </select>
                        </div>
                        <div class=\"form-group\"> 
                            <label for=\"category\">Product name</label> 
                            <input type=\"text\" class=\"form-control\" required=\"required\" id=\"product_name\" name=\"product\" placeholder=\"Product name...\"> 
                        </div>
                        <div class=\"form-group\"> 
                            <label for=\"category\">Product price</label> 
                            <input type=\"number\" class=\"form-control\" required=\"required\" id=\"product_price\" name=\"price\" placeholder=\"Product price...\"> 
                        </div>
                    </form>
                </div>
                <div class=\"modal-footer\">
                    <button type=\"button\" class=\"btn btn-secondary\" data-dismiss=\"modal\">Close</button>
                    <button type=\"button\" class=\"btn btn-primary\" onclick=\"Products.addEdit()\">Save changes</button>
                </div>
            </div>
        </div>
    </div>      
{% endblock %}

{% block javascripts %}
    <script type=\"text/javascript\">
        var _add_product = '{{path('products_new')}}';
        var _load_product = '{{path('products_load')}}';
        var _delete_product = '{{path('products_remove', { 'id': 0}) }}'
      
    </script>
    <script src=\"{{ asset('bundles/categories/js/jquery.min.js') }}\"></script>
    <script src=\"{{ asset('bundles/categories/js/tether.min.js') }}\"></script>
    <script src=\"{{ asset('bundles/categories/js/bootstrap.min.js') }}\"></script>
    <script src=\"{{ asset('bundles/products/js/functions.js') }}\"></script>
{% endblock  %}

", "ProductsBundle:products:index.html.twig", "C:\\xampp\\htdocs\\products\\src\\ProductsBundle/Resources/views/products/index.html.twig");
    }
}
