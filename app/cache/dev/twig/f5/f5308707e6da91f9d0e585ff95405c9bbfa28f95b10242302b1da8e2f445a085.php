<?php

/* :products:show.html.twig */
class __TwigTemplate_7979826cb05ac5eeb3c126fb6432dea07dfa2c5f73e4b6f58726b4bbf916f3b0 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("base.html.twig", ":products:show.html.twig", 1);
        $this->blocks = array(
            'body' => array($this, 'block_body'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a0ff20f3286f0f6f53bd5ca3f1633037c14263b9fde5bcf04ad5a7f26c843275 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a0ff20f3286f0f6f53bd5ca3f1633037c14263b9fde5bcf04ad5a7f26c843275->enter($__internal_a0ff20f3286f0f6f53bd5ca3f1633037c14263b9fde5bcf04ad5a7f26c843275_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", ":products:show.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_a0ff20f3286f0f6f53bd5ca3f1633037c14263b9fde5bcf04ad5a7f26c843275->leave($__internal_a0ff20f3286f0f6f53bd5ca3f1633037c14263b9fde5bcf04ad5a7f26c843275_prof);

    }

    // line 3
    public function block_body($context, array $blocks = array())
    {
        $__internal_fd03708000bf766643ac395aca407323c7ac7a654caa7fc7ed659e38e9516f33 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_fd03708000bf766643ac395aca407323c7ac7a654caa7fc7ed659e38e9516f33->enter($__internal_fd03708000bf766643ac395aca407323c7ac7a654caa7fc7ed659e38e9516f33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        // line 4
        echo "    <h1>Product</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>";
        // line 10
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "id", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Categoryid</th>
                <td>";
        // line 14
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "categoryId", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Productname</th>
                <td>";
        // line 18
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "productName", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Productprice</th>
                <td>";
        // line 22
        echo twig_escape_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "productPrice", array()), "html", null, true);
        echo "</td>
            </tr>
            <tr>
                <th>Updatedat</th>
                <td>";
        // line 26
        if ($this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "updatedAt", array())) {
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, $this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "updatedAt", array()), "Y-m-d H:i:s"), "html", null, true);
        }
        echo "</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"";
        // line 33
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_index");
        echo "\">Back to the list</a>
        </li>
        <li>
            <a href=\"";
        // line 36
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("products_edit", array("id" => $this->getAttribute((isset($context["product"]) ? $context["product"] : $this->getContext($context, "product")), "id", array()))), "html", null, true);
        echo "\">Edit</a>
        </li>
        <li>
            ";
        // line 39
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_start');
        echo "
                <input type=\"submit\" value=\"Delete\">
            ";
        // line 41
        echo         $this->env->getExtension('Symfony\Bridge\Twig\Extension\FormExtension')->renderer->renderBlock((isset($context["delete_form"]) ? $context["delete_form"] : $this->getContext($context, "delete_form")), 'form_end');
        echo "
        </li>
    </ul>
";
        
        $__internal_fd03708000bf766643ac395aca407323c7ac7a654caa7fc7ed659e38e9516f33->leave($__internal_fd03708000bf766643ac395aca407323c7ac7a654caa7fc7ed659e38e9516f33_prof);

    }

    public function getTemplateName()
    {
        return ":products:show.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  105 => 41,  100 => 39,  94 => 36,  88 => 33,  76 => 26,  69 => 22,  62 => 18,  55 => 14,  48 => 10,  40 => 4,  34 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends 'base.html.twig' %}

{% block body %}
    <h1>Product</h1>

    <table>
        <tbody>
            <tr>
                <th>Id</th>
                <td>{{ product.id }}</td>
            </tr>
            <tr>
                <th>Categoryid</th>
                <td>{{ product.categoryId }}</td>
            </tr>
            <tr>
                <th>Productname</th>
                <td>{{ product.productName }}</td>
            </tr>
            <tr>
                <th>Productprice</th>
                <td>{{ product.productPrice }}</td>
            </tr>
            <tr>
                <th>Updatedat</th>
                <td>{% if product.updatedAt %}{{ product.updatedAt|date('Y-m-d H:i:s') }}{% endif %}</td>
            </tr>
        </tbody>
    </table>

    <ul>
        <li>
            <a href=\"{{ path('products_index') }}\">Back to the list</a>
        </li>
        <li>
            <a href=\"{{ path('products_edit', { 'id': product.id }) }}\">Edit</a>
        </li>
        <li>
            {{ form_start(delete_form) }}
                <input type=\"submit\" value=\"Delete\">
            {{ form_end(delete_form) }}
        </li>
    </ul>
{% endblock %}
", ":products:show.html.twig", "C:\\xampp\\htdocs\\products\\app/Resources\\views/products/show.html.twig");
    }
}
