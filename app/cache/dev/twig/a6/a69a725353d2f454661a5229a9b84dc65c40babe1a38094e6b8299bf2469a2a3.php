<?php

/* @Framework/Form/integer_widget.html.php */
class __TwigTemplate_fab0cbb0d66e0a93ef8d90f0610c6c5604426349e1b894e584302c6671b74adb extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_417fc7d18064bff680f41bbee2419d702c82c7a3aee533c6ea3ccc7cc41d5007 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_417fc7d18064bff680f41bbee2419d702c82c7a3aee533c6ea3ccc7cc41d5007->enter($__internal_417fc7d18064bff680f41bbee2419d702c82c7a3aee533c6ea3ccc7cc41d5007_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/integer_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
";
        
        $__internal_417fc7d18064bff680f41bbee2419d702c82c7a3aee533c6ea3ccc7cc41d5007->leave($__internal_417fc7d18064bff680f41bbee2419d702c82c7a3aee533c6ea3ccc7cc41d5007_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/integer_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'number')) ?>
", "@Framework/Form/integer_widget.html.php", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\integer_widget.html.php");
    }
}
