<?php

/* @Framework/Form/repeated_row.html.php */
class __TwigTemplate_726cdfa1ec127c1d74ebabc9c0fc0636e1b8c723505da01d054bb8e14fe338ae extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_9d13f7d4bb01923f23df43c5ba57763059cc51402f411bdb39033b995db01f56 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_9d13f7d4bb01923f23df43c5ba57763059cc51402f411bdb39033b995db01f56->enter($__internal_9d13f7d4bb01923f23df43c5ba57763059cc51402f411bdb39033b995db01f56_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/repeated_row.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_rows') ?>
";
        
        $__internal_9d13f7d4bb01923f23df43c5ba57763059cc51402f411bdb39033b995db01f56->leave($__internal_9d13f7d4bb01923f23df43c5ba57763059cc51402f411bdb39033b995db01f56_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/repeated_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_rows') ?>
", "@Framework/Form/repeated_row.html.php", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\repeated_row.html.php");
    }
}
