<?php

/* @Framework/Form/range_widget.html.php */
class __TwigTemplate_a4410b41597edd8a7e1fa558b86a947dff85fa9249f24e9ae2cef06436382c23 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_f4ec0d7d9d6be692ff50120613f61761ffa6d6ee1d78794a062fb0a09813beb6 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f4ec0d7d9d6be692ff50120613f61761ffa6d6ee1d78794a062fb0a09813beb6->enter($__internal_f4ec0d7d9d6be692ff50120613f61761ffa6d6ee1d78794a062fb0a09813beb6_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/range_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
";
        
        $__internal_f4ec0d7d9d6be692ff50120613f61761ffa6d6ee1d78794a062fb0a09813beb6->leave($__internal_f4ec0d7d9d6be692ff50120613f61761ffa6d6ee1d78794a062fb0a09813beb6_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/range_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'range'));
", "@Framework/Form/range_widget.html.php", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\range_widget.html.php");
    }
}
