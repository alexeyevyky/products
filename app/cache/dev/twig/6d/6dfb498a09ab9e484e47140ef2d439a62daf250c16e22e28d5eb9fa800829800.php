<?php

/* ::base.html.twig */
class __TwigTemplate_d0827b3839233194891574a88cbf4c24bf410de28705f19204b0c45ac0ba7efa extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'title' => array($this, 'block_title'),
            'stylesheets' => array($this, 'block_stylesheets'),
            'body' => array($this, 'block_body'),
            'javascripts' => array($this, 'block_javascripts'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_5476b2b4a334df5855aa289fd60bf432de4083b78aca882d27bb07cee65cf71f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_5476b2b4a334df5855aa289fd60bf432de4083b78aca882d27bb07cee65cf71f->enter($__internal_5476b2b4a334df5855aa289fd60bf432de4083b78aca882d27bb07cee65cf71f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "::base.html.twig"));

        // line 1
        echo "<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>";
        // line 5
        $this->displayBlock('title', $context, $blocks);
        echo "</title>
        ";
        // line 6
        $this->displayBlock('stylesheets', $context, $blocks);
        // line 11
        echo "        <link rel=\"icon\" type=\"image/x-icon\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("favicon.ico"), "html", null, true);
        echo "\" />
    </head>
    <body>
    ";
        // line 14
        $this->displayBlock('body', $context, $blocks);
        // line 15
        echo "    ";
        $this->displayBlock('javascripts', $context, $blocks);
        // line 20
        echo "</body>
</html>
";
        
        $__internal_5476b2b4a334df5855aa289fd60bf432de4083b78aca882d27bb07cee65cf71f->leave($__internal_5476b2b4a334df5855aa289fd60bf432de4083b78aca882d27bb07cee65cf71f_prof);

    }

    // line 5
    public function block_title($context, array $blocks = array())
    {
        $__internal_6e52b4defb83c74bec1b663972e580e878c1b1b64a927cdbc9e802474f162825 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_6e52b4defb83c74bec1b663972e580e878c1b1b64a927cdbc9e802474f162825->enter($__internal_6e52b4defb83c74bec1b663972e580e878c1b1b64a927cdbc9e802474f162825_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "title"));

        echo "Welcome!";
        
        $__internal_6e52b4defb83c74bec1b663972e580e878c1b1b64a927cdbc9e802474f162825->leave($__internal_6e52b4defb83c74bec1b663972e580e878c1b1b64a927cdbc9e802474f162825_prof);

    }

    // line 6
    public function block_stylesheets($context, array $blocks = array())
    {
        $__internal_f0e60898c927c419dbdf254c3f23bf54dcfd2dbf2bc82a27b32b89b8ce59369f = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_f0e60898c927c419dbdf254c3f23bf54dcfd2dbf2bc82a27b32b89b8ce59369f->enter($__internal_f0e60898c927c419dbdf254c3f23bf54dcfd2dbf2bc82a27b32b89b8ce59369f_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "stylesheets"));

        // line 7
        echo "            <link rel=\"stylesheet\" href=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/css/bootstrap.min.css"), "html", null, true);
        echo "\" />
            <link rel=\"stylesheet\" href=\"";
        // line 8
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/css/style.css"), "html", null, true);
        echo "\" />
            
        ";
        
        $__internal_f0e60898c927c419dbdf254c3f23bf54dcfd2dbf2bc82a27b32b89b8ce59369f->leave($__internal_f0e60898c927c419dbdf254c3f23bf54dcfd2dbf2bc82a27b32b89b8ce59369f_prof);

    }

    // line 14
    public function block_body($context, array $blocks = array())
    {
        $__internal_17c3eeb7bd14d052a57e53c0a4f61a913791ca820ff17d1f59050c15ad42bda2 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_17c3eeb7bd14d052a57e53c0a4f61a913791ca820ff17d1f59050c15ad42bda2->enter($__internal_17c3eeb7bd14d052a57e53c0a4f61a913791ca820ff17d1f59050c15ad42bda2_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "body"));

        
        $__internal_17c3eeb7bd14d052a57e53c0a4f61a913791ca820ff17d1f59050c15ad42bda2->leave($__internal_17c3eeb7bd14d052a57e53c0a4f61a913791ca820ff17d1f59050c15ad42bda2_prof);

    }

    // line 15
    public function block_javascripts($context, array $blocks = array())
    {
        $__internal_eadb4a683e273580f23794d4164b25aee85aa1899bbf69e6275f3f026fa3d47a = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_eadb4a683e273580f23794d4164b25aee85aa1899bbf69e6275f3f026fa3d47a->enter($__internal_eadb4a683e273580f23794d4164b25aee85aa1899bbf69e6275f3f026fa3d47a_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "javascripts"));

        // line 16
        echo "        <script src=\"";
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/jquery.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 17
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/tether.min.js"), "html", null, true);
        echo "\"></script>
        <script src=\"";
        // line 18
        echo twig_escape_filter($this->env, $this->env->getExtension('Symfony\Bridge\Twig\Extension\AssetExtension')->getAssetUrl("bundles/categories/js/bootstrap.min.js"), "html", null, true);
        echo "\"></script>
    ";
        
        $__internal_eadb4a683e273580f23794d4164b25aee85aa1899bbf69e6275f3f026fa3d47a->leave($__internal_eadb4a683e273580f23794d4164b25aee85aa1899bbf69e6275f3f026fa3d47a_prof);

    }

    public function getTemplateName()
    {
        return "::base.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  118 => 18,  114 => 17,  109 => 16,  103 => 15,  92 => 14,  82 => 8,  77 => 7,  71 => 6,  59 => 5,  50 => 20,  47 => 15,  45 => 14,  38 => 11,  36 => 6,  32 => 5,  26 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<!DOCTYPE html>
<html>
    <head>
        <meta charset=\"UTF-8\" />
        <title>{% block title %}Welcome!{% endblock %}</title>
        {% block stylesheets %}
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/categories/css/bootstrap.min.css') }}\" />
            <link rel=\"stylesheet\" href=\"{{ asset('bundles/categories/css/style.css') }}\" />
            
        {% endblock  %}
        <link rel=\"icon\" type=\"image/x-icon\" href=\"{{ asset('favicon.ico') }}\" />
    </head>
    <body>
    {% block body %}{% endblock %}
    {% block javascripts %}
        <script src=\"{{ asset('bundles/categories/js/jquery.min.js') }}\"></script>
        <script src=\"{{ asset('bundles/categories/js/tether.min.js') }}\"></script>
        <script src=\"{{ asset('bundles/categories/js/bootstrap.min.js') }}\"></script>
    {% endblock  %}
</body>
</html>
", "::base.html.twig", "C:\\xampp\\htdocs\\products\\app/Resources\\views/base.html.twig");
    }
}
