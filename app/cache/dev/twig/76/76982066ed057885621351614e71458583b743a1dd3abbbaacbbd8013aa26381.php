<?php

/* @WebProfiler/Collector/router.html.twig */
class __TwigTemplate_b85c11bc5737433e339f521899ae85a754f13c337c5b8a28e64a4530cda76013 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        // line 1
        $this->parent = $this->loadTemplate("@WebProfiler/Profiler/layout.html.twig", "@WebProfiler/Collector/router.html.twig", 1);
        $this->blocks = array(
            'toolbar' => array($this, 'block_toolbar'),
            'menu' => array($this, 'block_menu'),
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doGetParent(array $context)
    {
        return "@WebProfiler/Profiler/layout.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_11bf95d4db299f1f7381867346ef9e95f83d5d024b23993216b7b159ac9d487d = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_11bf95d4db299f1f7381867346ef9e95f83d5d024b23993216b7b159ac9d487d->enter($__internal_11bf95d4db299f1f7381867346ef9e95f83d5d024b23993216b7b159ac9d487d_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@WebProfiler/Collector/router.html.twig"));

        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_11bf95d4db299f1f7381867346ef9e95f83d5d024b23993216b7b159ac9d487d->leave($__internal_11bf95d4db299f1f7381867346ef9e95f83d5d024b23993216b7b159ac9d487d_prof);

    }

    // line 3
    public function block_toolbar($context, array $blocks = array())
    {
        $__internal_4211cc5ced6b2682b8677964a5d54e6b2a8556a3167edff1757b5ce43c088f93 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_4211cc5ced6b2682b8677964a5d54e6b2a8556a3167edff1757b5ce43c088f93->enter($__internal_4211cc5ced6b2682b8677964a5d54e6b2a8556a3167edff1757b5ce43c088f93_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "toolbar"));

        
        $__internal_4211cc5ced6b2682b8677964a5d54e6b2a8556a3167edff1757b5ce43c088f93->leave($__internal_4211cc5ced6b2682b8677964a5d54e6b2a8556a3167edff1757b5ce43c088f93_prof);

    }

    // line 5
    public function block_menu($context, array $blocks = array())
    {
        $__internal_d5ae7c6f779542ee9cd6b83abdfa6a41e0b8970c5d54db376f308cd2ae942903 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d5ae7c6f779542ee9cd6b83abdfa6a41e0b8970c5d54db376f308cd2ae942903->enter($__internal_d5ae7c6f779542ee9cd6b83abdfa6a41e0b8970c5d54db376f308cd2ae942903_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "menu"));

        // line 6
        echo "<span class=\"label\">
    <span class=\"icon\">";
        // line 7
        echo twig_include($this->env, $context, "@WebProfiler/Icon/router.svg");
        echo "</span>
    <strong>Routing</strong>
</span>
";
        
        $__internal_d5ae7c6f779542ee9cd6b83abdfa6a41e0b8970c5d54db376f308cd2ae942903->leave($__internal_d5ae7c6f779542ee9cd6b83abdfa6a41e0b8970c5d54db376f308cd2ae942903_prof);

    }

    // line 12
    public function block_panel($context, array $blocks = array())
    {
        $__internal_0b9f9737225689be9e958a8e3e9d9c4353bbe94ff9d61561f8fd25ce4f6d3a9b = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_0b9f9737225689be9e958a8e3e9d9c4353bbe94ff9d61561f8fd25ce4f6d3a9b->enter($__internal_0b9f9737225689be9e958a8e3e9d9c4353bbe94ff9d61561f8fd25ce4f6d3a9b_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        // line 13
        echo "    ";
        echo $this->env->getExtension('Symfony\Bridge\Twig\Extension\HttpKernelExtension')->renderFragment($this->env->getExtension('Symfony\Bridge\Twig\Extension\RoutingExtension')->getPath("_profiler_router", array("token" => (isset($context["token"]) ? $context["token"] : $this->getContext($context, "token")))));
        echo "
";
        
        $__internal_0b9f9737225689be9e958a8e3e9d9c4353bbe94ff9d61561f8fd25ce4f6d3a9b->leave($__internal_0b9f9737225689be9e958a8e3e9d9c4353bbe94ff9d61561f8fd25ce4f6d3a9b_prof);

    }

    public function getTemplateName()
    {
        return "@WebProfiler/Collector/router.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  73 => 13,  67 => 12,  56 => 7,  53 => 6,  47 => 5,  36 => 3,  11 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% extends '@WebProfiler/Profiler/layout.html.twig' %}

{% block toolbar %}{% endblock %}

{% block menu %}
<span class=\"label\">
    <span class=\"icon\">{{ include('@WebProfiler/Icon/router.svg') }}</span>
    <strong>Routing</strong>
</span>
{% endblock %}

{% block panel %}
    {{ render(path('_profiler_router', { token: token })) }}
{% endblock %}
", "@WebProfiler/Collector/router.html.twig", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle\\Resources\\views\\Collector\\router.html.twig");
    }
}
