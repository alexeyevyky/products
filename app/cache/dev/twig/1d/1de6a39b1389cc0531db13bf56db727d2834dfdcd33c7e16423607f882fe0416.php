<?php

/* @Framework/Form/search_widget.html.php */
class __TwigTemplate_e1b977cb0ffc2c5342692de95d7192d90512e897f57fe4de5d2eb4f2f308e296 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_a2ad9ec548b6842bb0d679562b8b988d4a2dfd779734eaeadc7c6256e9dd8d33 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_a2ad9ec548b6842bb0d679562b8b988d4a2dfd779734eaeadc7c6256e9dd8d33->enter($__internal_a2ad9ec548b6842bb0d679562b8b988d4a2dfd779734eaeadc7c6256e9dd8d33_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/search_widget.html.php"));

        // line 1
        echo "<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
";
        
        $__internal_a2ad9ec548b6842bb0d679562b8b988d4a2dfd779734eaeadc7c6256e9dd8d33->leave($__internal_a2ad9ec548b6842bb0d679562b8b988d4a2dfd779734eaeadc7c6256e9dd8d33_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/search_widget.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<?php echo \$view['form']->block(\$form, 'form_widget_simple', array('type' => isset(\$type) ? \$type : 'search')) ?>
", "@Framework/Form/search_widget.html.php", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\search_widget.html.php");
    }
}
