<?php

/* WebProfilerBundle:Profiler:ajax_layout.html.twig */
class __TwigTemplate_11abdc10846c37259c0d9233f728c3ef4191a0cc0cf74fee67da4bfe18b1e557 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
            'panel' => array($this, 'block_panel'),
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_2913971fa5cdcee1d406e3b66f0a1bbd43636b832c3a5c4222274216b0b131c5 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_2913971fa5cdcee1d406e3b66f0a1bbd43636b832c3a5c4222274216b0b131c5->enter($__internal_2913971fa5cdcee1d406e3b66f0a1bbd43636b832c3a5c4222274216b0b131c5_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "WebProfilerBundle:Profiler:ajax_layout.html.twig"));

        // line 1
        $this->displayBlock('panel', $context, $blocks);
        
        $__internal_2913971fa5cdcee1d406e3b66f0a1bbd43636b832c3a5c4222274216b0b131c5->leave($__internal_2913971fa5cdcee1d406e3b66f0a1bbd43636b832c3a5c4222274216b0b131c5_prof);

    }

    public function block_panel($context, array $blocks = array())
    {
        $__internal_3cd1d3387957d8bf9510b815fd4d768da4fbe094815ffcb5f9c3a519b16700c1 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_3cd1d3387957d8bf9510b815fd4d768da4fbe094815ffcb5f9c3a519b16700c1->enter($__internal_3cd1d3387957d8bf9510b815fd4d768da4fbe094815ffcb5f9c3a519b16700c1_prof = new Twig_Profiler_Profile($this->getTemplateName(), "block", "panel"));

        echo "";
        
        $__internal_3cd1d3387957d8bf9510b815fd4d768da4fbe094815ffcb5f9c3a519b16700c1->leave($__internal_3cd1d3387957d8bf9510b815fd4d768da4fbe094815ffcb5f9c3a519b16700c1_prof);

    }

    public function getTemplateName()
    {
        return "WebProfilerBundle:Profiler:ajax_layout.html.twig";
    }

    public function getDebugInfo()
    {
        return array (  23 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("{% block panel '' %}
", "WebProfilerBundle:Profiler:ajax_layout.html.twig", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\WebProfilerBundle/Resources/views/Profiler/ajax_layout.html.twig");
    }
}
