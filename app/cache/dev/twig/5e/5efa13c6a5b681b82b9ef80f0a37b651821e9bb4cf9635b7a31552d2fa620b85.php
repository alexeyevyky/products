<?php

/* @Framework/Form/button_row.html.php */
class __TwigTemplate_42a786019fd10beee7cb90ac02fb4a43f7972c233af5d3ccc92aea8db4955527 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $__internal_d8d88c70b4c61947a972a2cc215d9b6d7867f5789b2b4689617bc3cc840f3626 = $this->env->getExtension("Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension");
        $__internal_d8d88c70b4c61947a972a2cc215d9b6d7867f5789b2b4689617bc3cc840f3626->enter($__internal_d8d88c70b4c61947a972a2cc215d9b6d7867f5789b2b4689617bc3cc840f3626_prof = new Twig_Profiler_Profile($this->getTemplateName(), "template", "@Framework/Form/button_row.html.php"));

        // line 1
        echo "<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
";
        
        $__internal_d8d88c70b4c61947a972a2cc215d9b6d7867f5789b2b4689617bc3cc840f3626->leave($__internal_d8d88c70b4c61947a972a2cc215d9b6d7867f5789b2b4689617bc3cc840f3626_prof);

    }

    public function getTemplateName()
    {
        return "@Framework/Form/button_row.html.php";
    }

    public function getDebugInfo()
    {
        return array (  22 => 1,);
    }

    /** @deprecated since 1.27 (to be removed in 2.0). Use getSourceContext() instead */
    public function getSource()
    {
        @trigger_error('The '.__METHOD__.' method is deprecated since version 1.27 and will be removed in 2.0. Use getSourceContext() instead.', E_USER_DEPRECATED);

        return $this->getSourceContext()->getCode();
    }

    public function getSourceContext()
    {
        return new Twig_Source("<div>
    <?php echo \$view['form']->widget(\$form) ?>
</div>
", "@Framework/Form/button_row.html.php", "C:\\xampp\\htdocs\\products\\vendor\\symfony\\symfony\\src\\Symfony\\Bundle\\FrameworkBundle\\Resources\\views\\Form\\button_row.html.php");
    }
}
