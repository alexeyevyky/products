$(document).ready(function(){
    Categories.load();
    $('#categoryModal').on('hidden.bs.modal', function (e) {
        $('#category_id').val('');
        $('#category_name').val('');
    });
});
var Categories = (function () {
    return {
        close: function () {
            $('#categoryModal').modal('hide');
            $('#category_id').val('');
            $('#category_name').val('');
        },
        load: function(){
            $.ajax({
                type: "GET",
                url: _load_category,
                dataType: 'json',
                cache: false,
                beforeSend: function (xhr) {
                     $('table.table tbody').html('<p>Loading data ....</p>');
                },
                success: function (response) {
                    if (response.length) {
                        var _html = '';
                        for(i in response){
                            _html += '<tr>';
                                _html += '<td>'+response[i].id+'</td>';
                                _html += '<td>'+response[i].category+'</td>';
                                _html += '<td>'+Categories.timeConverter(response[i].updatedAt.timestamp)+'</td>';
                                _html += '<td>';
                                    _html += '<a class="btn btn-success" onclick="Categories.openModalForEdit('+response[i].id+', \''+response[i].category+'\')" href="javascript:void(0)">Edit</a>';
                                    _html += '&nbsp;&nbsp;&nbsp;';
                                    _html += '<a class="btn btn-danger" onclick="Categories.delete('+response[i].id+')" href="javascript:void(0)">Delete</a>';
                                _html += '</td>';
                            _html += '</tr>';
                        }
                        $('table.table tbody').html(_html);
                    }
                },
                error: function () {
                    alert('Error!');
                }
            });
        },
        addEdit:function(){
            if(typeof _add_category == 'undefined'){
                alert('Bad request!');
            }
            var data = {id: $('#category_id').val(), category : $('#category_name').val()};
            $.ajax({
                type: "POST",
                url: _add_category,
                dataType: 'json',
                data: data,
                cache: false,
                success: function (response) {
                    if (response.status == 1) {
                       Categories.close();
                       Categories.load();
                    }else{
                        var _alert = '<div id="categoty_alert" class="alert alert-warning">';
                        _alert += '<strong>Warning!</strong> '+response.message;
                        _alert += '</div>';
                        
                        $('#alerts').html(_alert);
                        setTimeout(function(){ 
                            $('#categoty_alert').slideUp(); 
                            $('#categoty_alert').remove(); 
                        }, 5000);
                    }
                },
                error: function () {
                    alert('Error!');
                }
            });
            
           
        },
        openModalForEdit: function(id, category){
            $('#category_id').val(id);
            $('#category_name').val(category);
            $('#categoryModal').modal('show');
        },
        delete: function(id){
            if(confirm('Are you sure you want to delete this category?')){
                var  url = _delete_category.replace("0",id);
                $.ajax({
                    type: "DELETE",
                    url: url,
                    dataType: 'json',
                    data: {},
                    cache: false,
                    success: function (response) {
                        if (response.status == 1) {
                            Categories.load();
                        } else {
                            alert(response.message);
                        }
                    },
                    error: function () {
                        alert('Error!');
                    }
                });
            }
            
        },
        timeConverter: function (UNIX_timestamp) {
            var a = new Date(UNIX_timestamp * 1000);
            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var year = a.getFullYear();
            var month = months[a.getMonth()];
            var date = a.getDate();
            var hour = a.getHours();
            var min = a.getMinutes();
            var sec = a.getSeconds();
            var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
            return time;
        }
    }
}());