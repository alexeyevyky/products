<?php

namespace ProductsBundle\Controller;

use ProductsBundle\Entity\Products;
use DateTime;
use CategoriesBundle\Entity\Categories;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Product controller.
 *
 * @Route("products")
 */
class ProductsController extends Controller
{
    /**
     * Lists all product entities.
     *
     * @Route("/", name="products_index")
     * @Method("GET")
     */
    public function indexAction()
    {
         $em = $this->getDoctrine()->getManager();
         $categories = $em->getRepository('CategoriesBundle:Categories')->findAll();
         return $this->render('@Products/products/index.html.twig', array('categories' => $categories));
    }
    
     /**
     * Lists all category entities.
     *
     * @Route("/load", name="products_load")
     * @Method("GET")
     */
    public function loadAction()
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository('ProductsBundle:Products')->findAll();
       
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        die($serializer->serialize($products, 'json'));
    }

    /**
     * Creates a new product entity.
     *
     * @Route("/new", name="products_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
       if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('status' => 0, 'message' => 'You can access this only using Ajax!'), 400);
        }
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        # if edit request
        if(!empty($data['id'])){
            $product = $em->getRepository('ProductsBundle:Products')->find($data['id']);
            if($product){
                $product->setProductName($data['product']);
                $product->setProductPrice($data['price']);
                $em->persist($product);
                $em->flush($product);
            return new JsonResponse(array('status' => 1,'message' => 'Succes', 'last_id' => $product->getId()), 200);
            }
        }
        $existingProduct = $em->getRepository('ProductsBundle:Products')->findOneBy(array('productName' => $data['product']));
        if($existingProduct){
            return new JsonResponse(array('status'  =>  '0', 'message' => 'This product already exists!'));
        }
        
        $validate = $this->validateData($data);
        if($validate !== true){
             return new JsonResponse(array('status'  =>  '0', 'message' => $validate));
        }
        
        $categories = $em->getRepository('CategoriesBundle:Categories')->find($data['category_id']);
        if(!$categories){
            return new JsonResponse(array('status'  =>  '0', 'message' => 'This category does not exists!'));
        }
        try{
            $product = new Products();
            $product->setCategoryId($categories);
            $product->setProductName($data['product']);
            $product->setProductPrice($data['price']);
            $product->setUpdatedAt(new \DateTime());
            
            $em->persist($product);
            $em->flush($product);
            return new JsonResponse(array('status' => 1,'message' => 'Succes', 'last_id' => $product->getId()), 200);
        }  catch (Exception $e){
            return new JsonResponse(array('status'  =>  '0', 'message' => $e->getMessage()));
        }
        
    }
    
     /**
     * @param $data
     * @return bool
     */
    protected function validateData($data)
    {
        if(!array_key_exists('category_id', $data) || !$data['category_id']){
            return 'Category is required!';           
        }
        if(!array_key_exists('product', $data) || !$data['product']){
            return 'Product name is required!';   
         }
        if(!array_key_exists('price', $data) || !$data['price']){
            return 'Product price is required!';   
        }       
        return true;
    }

    /**
     * Finds and displays a product entity.
     *
     * @Route("/{id}", name="products_show")
     * @Method("GET")
     */
    public function showAction(Products $product)
    {
        $deleteForm = $this->createDeleteForm($product);

        return $this->render('products/show.html.twig', array(
            'product' => $product,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing product entity.
     *
     * @Route("/{id}/edit", name="products_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Products $product)
    {
        $deleteForm = $this->createDeleteForm($product);
        $editForm = $this->createForm('ProductsBundle\Form\ProductsType', $product);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('products_edit', array('id' => $product->getId()));
        }

        return $this->render('products/edit.html.twig', array(
            'product' => $product,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a product entity.
     *
     * @Route("/{id}", name="products_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Products $product)
    {
        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush($product);
        }

        return $this->redirectToRoute('products_index');
    }
    
    /**
     * Deletes a products entity.
     *
     * @Route("/remove/{id}", name="products_remove")
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('status' => 0, 'message' => 'You can access this only using Ajax!'), 400);
        }
        $em = $this->getDoctrine()->getManager();
        $product = $em->getRepository('ProductsBundle:Products')->find($id);
        if(!$product){
            return new JsonResponse(array('status' => 0, 'message' => 'This product does not exists!'), 400);
        }
        
        $em->remove($product);
        $em->flush($product);
        return new JsonResponse(array('status' => 1,'message' => 'Succes'), 200);
    }

    /**
     * Creates a form to delete a product entity.
     *
     * @param Products $product The product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Products $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('products_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
