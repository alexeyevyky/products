<?php

namespace ProductsBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use DateTime;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use CategoriesBundle\Entity\Categories;

/**
 * Products
 *
 * @ORM\Table(name="products")
 * @UniqueEntity("product_name", message="The product name already exist!")
 * @ORM\Entity(repositoryClass="ProductsBundle\Repository\ProductsRepository")
 */
class Products
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @ORM\ManyToOne(targetEntity="CategoriesBundle\Entity\Categories", inversedBy="categoryId")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id", nullable=FALSE, onDelete="CASCADE")
     */
    private $category;

    /**
     * @var string
     *
     * @ORM\Column(name="product_name", type="string", length=255, unique=true)
     */
    private $productName;

    /**
     * @var int
     *
     * @ORM\Column(name="product_price", type="decimal", precision=10, scale=4)
     */
    private $productPrice;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="updated_at", type="datetime", nullable=true, options={"default":"CURRENT_TIMESTAMP"})
     */
    private $updatedAt;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

     /**
     * 
     * @param Categories $categories
     * @return \CategoriesBundle\Entity\Categories
     */
    public function setCategoryId(Categories $categories)
    {
        $this->category = $categories;

        return $this;
    }

    /**
     * Get categoryId
     *
     * @return int
     */
    public function getCategoryId()
    {
        return $this->category;
    }

    /**
     * Set productName
     *
     * @param string $productName
     *
     * @return Products
     */
    public function setProductName($productName)
    {
        $this->productName = $productName;

        return $this;
    }

    /**
     * Get productName
     *
     * @return string
     */
    public function getProductName()
    {
        return $this->productName;
    }

    /**
     * Set productPrice
     *
     * @param integer $productPrice
     *
     * @return Products
     */
    public function setProductPrice($productPrice)
    {
        $this->productPrice = $productPrice;

        return $this;
    }

    /**
     * Get productPrice
     *
     * @return int
     */
    public function getProductPrice()
    {
        return $this->productPrice;
    }

    /**
     * Set upda
     *
     * @param \DateTime $updatedAt
     *
     * @return Products
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }
}

