$(document).ready(function(){
    Products.load();
    $('#productModal').on('hidden.bs.modal', function (e) {
        $('#product_id').val('');
        $('#product_name').val('');
    });
});
var Products = (function () {
    return {
        close: function () {
            $('#productModal').modal('hide');
            $('#product_id').val('');
            $('#product_name').val('');
        },
        load: function(){
            $.ajax({
                type: "GET",
                url: _load_product,
                dataType: 'json',
                cache: false,
                beforeSend: function (xhr) {
                     $('table.table tbody').html('<p>Loading data ....</p>');
                },
                success: function (response) {
                    if (response.length) {
                        var _html = '';
                        for(i in response){
                            _html += '<tr>';
                                _html += '<td>'+response[i].id+'</td>';
                                _html += '<td>'+response[i].categoryId.category+'</td>';
                                _html += '<td>'+response[i].productName+'</td>';
                                _html += '<td>'+response[i].productPrice+'</td>';
                                _html += '<td>'+Products.timeConverter(response[i].updatedAt.timestamp)+'</td>';
                                _html += '<td>';
                                    _html += '<a class="btn btn-success" onclick="Products.openModalForEdit('+response[i].id+','+response[i].categoryId.id+', \''+response[i].productName+'\', \''+response[i].productPrice+'\')" href="javascript:void(0)">Edit</a>';
                                    _html += '&nbsp;&nbsp;&nbsp;';
                                    _html += '<a class="btn btn-danger" onclick="Products.delete('+response[i].id+')" href="javascript:void(0)">Delete</a>';
                                _html += '</td>';
                            _html += '</tr>';
                        }
                        $('table.table tbody').html(_html);
                    }else{
                        $('table.table tbody').html('<p>Empty products table!</p>');
                    }
                },
                error: function () {
                    alert('Error!');
                }
            });
        },
        addEdit:function(){
            if(typeof _add_product == 'undefined'){
                alert('Bad request!');
            }
            
            var data = {
                id: $('#product_id').val(), 
                category_id : $('#category_id').val(),
                product : $('#product_name').val(),
                price : $('#product_price').val()
            };
            $.ajax({
                type: "POST",
                url: _add_product,
                dataType: 'json',
                data: data,
                cache: false,
                success: function (response) {
                    if (response.status == 1) {
                       Products.close();
                       Products.load();
                    }else{
                        var _alert = '<div id="product_alert" class="alert alert-warning">';
                        _alert += '<strong>Warning!</strong> '+response.message;
                        _alert += '</div>';
                        
                        $('#alerts').html(_alert);
                        setTimeout(function(){ 
                            $('#product_alert').slideUp(); 
                            $('#product_alert').remove(); 
                        }, 5000);
                    }
                },
                error: function () {
                    alert('Error!');
                }
            });
            
           
        },
        openModalForEdit: function(id, category, product, price){
            $('#product_id').val(id);
            $('#category_id').val(category);
            $('#product_name').val(product);
            $('#product_price').val(price);
            $('#productModal').modal('show');
        },
        delete: function(id){
            if(confirm('Are you sure you want to delete this product?')){
                var  url = _delete_product.replace("0",id);
                $.ajax({
                    type: "DELETE",
                    url: url,
                    dataType: 'json',
                    data: {},
                    cache: false,
                    success: function (response) {
                        if (response.status == 1) {
                            Products.load();
                        } else {
                            alert(response.message);
                        }
                    },
                    error: function () {
                        alert('Error!');
                    }
                });
            }
            
        },
        timeConverter: function (UNIX_timestamp) {
            var a = new Date(UNIX_timestamp * 1000);
            var months = ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'];
            var year = a.getFullYear();
            var month = months[a.getMonth()];
            var date = a.getDate();
            var hour = a.getHours();
            var min = a.getMinutes();
            var sec = a.getSeconds();
            var time = date + ' ' + month + ' ' + year + ' ' + hour + ':' + min + ':' + sec;
            return time;
        }
    }
}());