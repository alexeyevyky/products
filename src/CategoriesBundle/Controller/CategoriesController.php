<?php

namespace CategoriesBundle\Controller;

use DateTime;
use CategoriesBundle\Entity\Categories;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Symfony\Component\Serializer\Serializer;
use Symfony\Component\Serializer\Encoder\XmlEncoder;
use Symfony\Component\Serializer\Encoder\JsonEncoder;
use Symfony\Component\Serializer\Normalizer\ObjectNormalizer;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Method;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Category controller.
 *
 * @Route("categories")
 */
class CategoriesController extends Controller
{
    /**
     * Lists all category entities.
     *
     * @Route("/", name="categories_index")
     * @Method("GET")
     */
    public function indexAction()
    {
       return $this->render('@Categories/categories/index.html.twig');
    }
    
    /**
     * Lists all category entities.
     *
     * @Route("/load", name="categories_load")
     * @Method("GET")
     */
    public function loadAction()
    {
        $em = $this->getDoctrine()->getManager();

        $categories = $em->getRepository('CategoriesBundle:Categories')->findAll();
        
        $encoders = array(new XmlEncoder(), new JsonEncoder());
        $normalizers = array(new ObjectNormalizer());
        $serializer = new Serializer($normalizers, $encoders);
        die($serializer->serialize($categories, 'json'));
    }

    /**
     * Creates a new category entity.
     *
     * @Route("/new", name="categories_new")
     * @Method({"GET", "POST"})
     */
    public function newAction(Request $request)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('status' => 0, 'message' => 'You can access this only using Ajax!'), 400);
        }
        $data = $request->request->all();
        $em = $this->getDoctrine()->getManager();
        
        # if edit request
        if(!empty($data['id'])){
            $category = $em->getRepository('CategoriesBundle:Categories')->find($data['id']);
            if($category){
                $category->setCategory($data['category']);
                $em->persist($category);
                $em->flush($category);
            return new JsonResponse(array('status' => 1,'message' => 'Succes', 'last_id' => $data['id']), 200);
            }
        }
        
        $existingCategory = $em->getRepository('CategoriesBundle:Categories')->findOneBy(array('category' => $data['category']));
        if($existingCategory){
            return new JsonResponse(array('status'  =>  '0', 'message' => 'This category already exists!'));
        }
        # validate is not empty category field
        if(!array_key_exists('category', $data) || !$data['category']){
            return 'Category is required!';           
        }
        
        try{
            $category = new Categories();
            $category->setCategory($data['category']);
            $category->setUpdatedAt(new \DateTime());
            
            $em->persist($category);
            $em->flush($category);
            return new JsonResponse(array('status' => 1,'message' => 'Succes', 'last_id' => $category->getId()), 200);
        }  catch (Exception $e){
            return new JsonResponse(array('status'  =>  '0', 'message' => $e->getMessage()));
        }
        

    }

    /**
     * Finds and displays a category entity.
     *
     * @Route("/{id}", name="categories_show")
     * @Method("GET")
     */
    public function showAction(Categories $category)
    {
        $deleteForm = $this->createDeleteForm($category);

        return $this->render('categories/show.html.twig', array(
            'category' => $category,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing category entity.
     *
     * @Route("/{id}/edit", name="categories_edit")
     * @Method({"GET", "POST"})
     */
    public function editAction(Request $request, Categories $category)
    {
        $deleteForm = $this->createDeleteForm($category);
        $editForm = $this->createForm('CategoriesBundle\Form\CategoriesType', $category);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('categories_edit', array('id' => $category->getId()));
        }

        return $this->render('categories/edit.html.twig', array(
            'category' => $category,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a category entity.
     *
     * @Route("/{id}", name="categories_delete")
     * @Method("DELETE")
     */
    public function deleteAction(Request $request, Categories $category)
    {
        $form = $this->createDeleteForm($category);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($category);
            $em->flush($category);
        }

        return $this->redirectToRoute('categories_index');
    }
    
     /**
     * Deletes a category entity.
     *
     * @Route("/remove/{id}", name="categories_remove")
     * @Method("DELETE")
     */
    public function removeAction(Request $request, $id)
    {
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('status' => 0, 'message' => 'You can access this only using Ajax!'), 400);
        }
        $em = $this->getDoctrine()->getManager();
        $category = $em->getRepository('CategoriesBundle:Categories')->find($id);
        if(!$category){
            return new JsonResponse(array('status' => 0, 'message' => 'This category does not exists!'), 400);
        }
        
        $em->remove($category);
        $em->flush($category);
        return new JsonResponse(array('status' => 1,'message' => 'Succes'), 200);
    }

    /**
     * Creates a form to delete a category entity.
     *
     * @param Categories $category The category entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Categories $category)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('categories_delete', array('id' => $category->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
