<?php

namespace CategoriesBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use CategoriesBundle\Entity\Categories;
use DateTime;

class LoadCategoryData implements FixtureInterface
{
    public function load(ObjectManager $manager)
    {
        $category = new Categories();
        $category->setCategory('Shoes');
        $category->setUpdatedAt(new \DateTime());

        $manager->persist($category);
        $manager->flush();
        
        $category = new Categories();
        $category->setCategory('Clothes');
        $category->setUpdatedAt(new \DateTime());

        $manager->persist($category);
        $manager->flush();
    }
}